// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Engine/GameViewportClient.h"
#include "Engine/Engine.h"
#include "WtcViewportClient.generated.h"

/**
 *
 */
UCLASS()
class WHATTHECLUCK_API UWtcViewportClient : public UGameViewportClient
{
	GENERATED_BODY()

public:
	virtual bool InputKey(FViewport* Viewport, int32 ControllerId, FKey Key, EInputEvent EventType, float AmountDepressed = 1.f, bool bGamepad = false) override;
	virtual bool InputAxis(FViewport* Viewport, int32 ControllerId, FKey Key, float Delta, float DeltaTime, int32 NumSamples = 1, bool bGamepad = false) override;

	void SetDisableSplitscreen(bool IsEnabled);
};
