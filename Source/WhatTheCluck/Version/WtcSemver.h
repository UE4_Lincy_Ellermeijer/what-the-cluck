#pragma once

#include "WhatTheCluck.h"

enum class ESemVerPart
{
	Major,
	Minor,
	Patch,
	PreRelease,
	BuildMeta
};

class FSxSemVer
{
public:

	FSxSemVer(uint32 Major = 0, uint32 Minor = 0, uint32 Patch = 0);

	void AddPreReleaseIdentifier(const FString& PreReleaseIdentifier);
	void AddBuildMetaData(const FString& MetaData);

	// Returns the concatenated semver string, up to the given point
	// A full example would be: 1.2.3-alpha.1+f536b8.4-12.2016-5-1
	// The pre-release is separated from the version with a -, each pre-release is separated with a .
	// The build meta data is separated from the pre-release with a + and each metadata is separated with a . as well.
	FString ToString(ESemVerPart LastPart = ESemVerPart::BuildMeta) const;

	FString GetBuildMetaDataString() const;

	const TArray<FString>& GetPreReleaseIdentifiers() const;
	const TArray<FString>& GetBuildMetaData() const;

	uint32 GetMajor() const;
	uint32 GetMinor() const;
	uint32 GetPatch() const;

protected:
private:

	uint32 MajorVersion;
	uint32 MinorVersion;
	uint32 PatchVersion;

	TArray<FString> PreReleaseIdentifiers;
	TArray<FString> BuildMetaData;
};
