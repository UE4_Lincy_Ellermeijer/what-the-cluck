#include "WhatTheCluck.h"
#include "WtcSemver.h"

FSxSemVer::FSxSemVer(uint32 Major /*= 0*/, uint32 Minor /*= 0*/, uint32 Patch /*= 0*/) :
	MajorVersion(Major),
	MinorVersion(Minor),
	PatchVersion(Patch),
	PreReleaseIdentifiers(),
	BuildMetaData()
{

}

void FSxSemVer::AddPreReleaseIdentifier(const FString& PreReleaseIdentifier)
{
	PreReleaseIdentifiers.Add(PreReleaseIdentifier);
}

void FSxSemVer::AddBuildMetaData(const FString& MetaData)
{
	BuildMetaData.Add(MetaData);
}

FString FSxSemVer::ToString(ESemVerPart LastPart /*= ESemVerPart::BuildMeta*/) const
{
	FString Result;
	Result += FString::FromInt(GetMajor());
	Result += TEXT(".");
	if (LastPart != ESemVerPart::Major)
	{
		Result += FString::FromInt(GetMinor());
		Result += TEXT(".");
		if (LastPart != ESemVerPart::Minor)
		{
			Result += FString::FromInt(GetPatch());
			if (LastPart != ESemVerPart::Patch)
			{
				if (PreReleaseIdentifiers.Num() != 0)
				{
					Result += TEXT("-");
					Result += PreReleaseIdentifiers[0];
					for (int32 PreReleaseIndex = 1; PreReleaseIndex < PreReleaseIdentifiers.Num(); ++PreReleaseIndex)
					{
						Result += TEXT(".");
						Result += PreReleaseIdentifiers[PreReleaseIndex];
					}
				}
				if (LastPart != ESemVerPart::PreRelease)
				{
					if (BuildMetaData.Num() != 0)
					{
						Result += TEXT("+");
						Result += BuildMetaData[0];
						for (int32 BuildMetaDataIndex = 1; BuildMetaDataIndex < BuildMetaData.Num(); ++BuildMetaDataIndex)
						{
							Result += TEXT(".");
							Result += BuildMetaData[BuildMetaDataIndex];
						}
					}
				}
			}
		}
	}
	return Result;
}

FString FSxSemVer::GetBuildMetaDataString() const
{
	return FString::Join(BuildMetaData, TEXT("."));
}

const TArray<FString>& FSxSemVer::GetPreReleaseIdentifiers() const
{
	return PreReleaseIdentifiers;
}

const TArray<FString>& FSxSemVer::GetBuildMetaData() const
{
	return BuildMetaData;
}

uint32 FSxSemVer::GetMajor() const
{
	return MajorVersion;
}

uint32 FSxSemVer::GetMinor() const
{
	return MinorVersion;
}

uint32 FSxSemVer::GetPatch() const
{
	return PatchVersion;
}
