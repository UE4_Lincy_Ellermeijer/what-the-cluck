#include "WhatTheCluck.h"
#include "WtcVersionWidget.h"
#include "Engine/Font.h"

void SSxVersionWidget::Construct(const FArguments& InArgs)
{
	const FSxVersion& Version = InArgs._Version;

	UFont* Font = LoadObject<UFont>(nullptr, TEXT("Font'/Game/UserInterface/DejaVuSansCondensed.DejaVuSansCondensed'"), nullptr);

	// We display this widget all the time, so we can safely add the used font to the root.
	Font->AddToRoot();

	Visibility = EVisibility::HitTestInvisible;

	ChildSlot
	.VAlign(VAlign_Bottom)
	.HAlign(HAlign_Left)
	[
		SNew(SVerticalBox)
		.Visibility(EVisibility::HitTestInvisible)
		+SVerticalBox::Slot()
		.HAlign(HAlign_Left)
		[
			SNew(STextBlock)
			.ColorAndOpacity(FLinearColor::White)
			.ShadowColorAndOpacity(FLinearColor::Black)
			.ShadowOffset(FIntPoint(-1, 1))
			.Font(FSlateFontInfo(Font, 12))
			.Text(FText::FromString(Version.GetGameVersionString()))
		]
	];
}
