#pragma once


#include "WhatTheCluck.h"
#include "SlateBasics.h"
#include "WtcVersion.h"


class SSxVersionWidget : public SCompoundWidget
{
	SLATE_BEGIN_ARGS(SSxVersionWidget){}
		SLATE_ARGUMENT(FSxVersion, Version)
		SLATE_ARGUMENT(FString, UserID)
	SLATE_END_ARGS()

public:

	void Construct(const FArguments& InArgs);

};
