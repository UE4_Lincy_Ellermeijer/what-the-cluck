#pragma once

#include "WhatTheCluck.h"
#include "WtcSemver.h"
#include "EngineVersion.h"

struct FSxVersion
{
	FSxVersion();

	FSxSemVer GameVersion;
	FEngineVersion EngineVersion;
	FSxSemVer CustomEngineVersion;
	uint32 ProceduralAlgorithmId;

	FString GetGameVersionString() const;
	FString GetEngineVersionString() const;
	FString GetProceduralAlgorithmString() const;
};

static FSxVersion GVersion;
