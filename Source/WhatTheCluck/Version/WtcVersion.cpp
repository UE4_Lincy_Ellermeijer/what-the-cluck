#include "WhatTheCluck.h"
#include "WtcVersion.h"
#include "WtcGeneratedVersion.h"


FSxVersion::FSxVersion()
{
	GameVersion = FSxSemVer(SX_GAME_VERSION_MAJOR, SX_GAME_VERSION_MINOR, SX_GAME_VERSION_PATCH);
	const FString PreReleaseList[] = SX_PREBUILD_STRING;
	TArray<FString> BuildserverPreReleaseIdentifiers;
	BuildserverPreReleaseIdentifiers.Append(PreReleaseList, ARRAY_COUNT(PreReleaseList));
	if (BuildserverPreReleaseIdentifiers.Num() != 0)
	{
		for (auto Identifier : BuildserverPreReleaseIdentifiers)
		{
			if (Identifier.Len() != 0)
			{
				GameVersion.AddPreReleaseIdentifier(Identifier);
			}
		}
	}
#if UE_BUILD_DEBUG
	GameVersion.AddBuildMetaData(TEXT("Debug"));
#endif
#if UE_BUILD_DEVELOPMENT
	GameVersion.AddBuildMetaData(TEXT("Development"));
#endif
#if UE_BUILD_TEST
	GameVersion.AddBuildMetaData(TEXT("Test"));
#endif
#if UE_BUILD_SHIPPING
	GameVersion.AddBuildMetaData(TEXT("Shipping"));
#endif
#if WITH_EDITOR
	GameVersion.AddBuildMetaData(TEXT("Editor"));
#endif

	const FString BuildMetaList[] = SX_BUILDMETA_STRING;
	TArray<FString> BuildserverBuildMetaIdentifiers;
	BuildserverBuildMetaIdentifiers.Append(BuildMetaList, ARRAY_COUNT(BuildMetaList));
	if (BuildserverBuildMetaIdentifiers.Num() != 0)
	{
		for (auto Identifier : BuildserverBuildMetaIdentifiers)
		{
			if (Identifier.Len() != 0)
			{
				GameVersion.AddBuildMetaData(Identifier);
			}
		}
	}

	EngineVersion = FEngineVersion::Current();
	CustomEngineVersion = FSxSemVer(SX_ENGINE_VERSION_MAJOR, SX_ENGINE_VERSION_MINOR, SX_ENGINE_VERSION_PATCH);
	ProceduralAlgorithmId = 0;
}

FString FSxVersion::GetGameVersionString() const
{
	return TEXT("Game: ") + GameVersion.ToString();
}

FString FSxVersion::GetEngineVersionString() const
{
	return TEXT("Engine: ") + EngineVersion.ToString(EVersionComponent::Changelist) + TEXT(" Custom: ") + CustomEngineVersion.ToString();
}

FString FSxVersion::GetProceduralAlgorithmString() const
{
	return TEXT("Procedural: ") + FString::FromInt(ProceduralAlgorithmId);
}
