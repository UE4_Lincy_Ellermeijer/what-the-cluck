// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class WhatTheCluck : ModuleRules
{
  public WhatTheCluck(TargetInfo Target)
  {
    PrivateDependencyModuleNames.AddRange(new string[]
    {
      "Sockets",
      "Networking"
    });

    PublicDependencyModuleNames.AddRange(new string[]
    {
      "Slate",
      "SlateCore",
      "Core",
      "CoreUObject",
      "Engine",
      "InputCore",
      "UMG",
      "Paper2D"
    });

    PrivateIncludePaths.AddRange(new string[]
    {
      "WhatTheCluck/Utils",
      "WhatTheCluck/Version",
      "WhatTheCluck/Game",
      "WhatTheCluck/Game/GameMode",
    });

    bFasterWithoutUnity = true;
  }
}
