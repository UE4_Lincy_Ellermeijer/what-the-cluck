// Fill out your copyright notice in the Description page of Project Settings.

#include "WhatTheCluck.h"
#include "WtcCharacter.h"
#include "WtcMovementComponent.h"
#include "WtcAbilityComponent.h"
#include "WtcGameState.h"
#include "WtcTeam.h"

UWtcAbilityComponent::UWtcAbilityComponent()
{
	PrimaryComponentTick.bCanEverTick = true;
	bReplicates = true;
}

void UWtcAbilityComponent::BeginPlay()
{
	Super::BeginPlay();
}

void UWtcAbilityComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	if (bIsPassive && PassiveConditional())
	{
		ExecuteAbility();
	}
	else if (bIsPassive)
	{
		ResetAbility();
	}
}

void UWtcAbilityComponent::ActivateAbility()
{
	if (!DurationDelegate.IsBound())
	{
		DurationDelegate.BindLambda([this]
		{
			ResetAbility();
		});
	}
	if (!CooldownDelegate.IsBound())
	{
		CooldownDelegate.BindLambda([this]
		{
			bCanUseAbility = true;
		});
	}

	if (bCanUseAbility && GetOwnerCharacter()->bCanUseStaminaAbilities && !bIsPassive)
	{
		GetOwnerCharacter()->RemoveStamina(StaminaCost);
		ExecuteAbility();
		AWtcGameState* const GameState = GetWorld()->GetGameState<AWtcGameState>();
		if (GetOwnerCharacter()->Team == EWtcTeam::WTC_Red) {
			GameState->RedStatistics.AbilitiesUsed++;
		} else {
			GameState->BlueStatistics.AbilitiesUsed++;
		}
		bCanUseAbility = false;
		GetOwnerCharacter()->GetWorldTimerManager().SetTimer(DurationTimer, DurationDelegate, Duration, false);
		GetOwnerCharacter()->GetWorldTimerManager().SetTimer(CooldownTimer, CooldownDelegate, Cooldown, false);
	}

}

void UWtcAbilityComponent::DeActivateAbility()
{

}

AWtcCharacter* UWtcAbilityComponent::GetOwnerCharacter()
{
	return Cast<AWtcCharacter>(GetOwner());
}

UWtcMovementComponent* UWtcAbilityComponent::GetOwnerMovementComponent()
{
	return Cast<UWtcMovementComponent>(GetOwnerCharacter()->GetMovementComponent());
}
