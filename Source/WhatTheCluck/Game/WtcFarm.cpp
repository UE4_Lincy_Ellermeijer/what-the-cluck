// Fill out your copyright notice in the Description page of Project Settings.

#include "WhatTheCluck.h"
#include "WtcGameState.h"
#include "WtcFarm.h"
#include "UnrealNetwork.h"
#include "WtcGameMode.h"

// Sets default values
AWtcFarm::AWtcFarm()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	EggRespawnDelegate.BindLambda([this]()
	{
		UWorld* World = GetWorld();
		if (World)
		{
			AWtcGameState* GameState = Cast<AWtcGameState>(World->GetGameState());
			if (GameState->bIsInPlayingState && CurrentAmountOfEggs < MaximumAmountOfEggs && HasAuthority())
			{
				SpawnEgg();
			}
		}
	});
}

// Called when the game starts or when spawned
void AWtcFarm::BeginPlay()
{
	Super::BeginPlay();
	GetWorldTimerManager().SetTimer(EggRespawnTimer, EggRespawnDelegate, EggRespawnTime, true);
}

void AWtcFarm::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	GetWorldTimerManager().PauseTimer(EggRespawnTimer);
}

// Called every frame
void AWtcFarm::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	for (FRoosterSniper Sniper : PendingRoosterSnipers)
	{

		if (Sniper.SniperReticleActor)
		{
			// Move Actor
			Sniper.SniperReticleActor->SetActorLocation(Sniper.Rooster->GetActorLocation());
		}

	}
}

void AWtcFarm::AddSniperToRooster(AWtcChicken* Rooster)
{
	if (!HasSniperAttached(Rooster))
	{
		FRoosterSniper RoosterSniper;
		RoosterSniper.Rooster = Rooster;
		// Spawn Actor
		FVector Location = RoosterSniper.Rooster->GetActorLocation();
		FRotator Rotation = FRotator::ZeroRotator;
		RoosterSniper.SniperReticleActor = GetWorld()->SpawnActor(SniperActorClass, &Location, &Rotation);

		FTimerDelegate SniperDelegate;
		SniperDelegate.BindLambda([Rooster, this]()
		{
			if (Rooster->GetChickenState() == EWtcChickenState::WTC_IS_CARRIED)
			{
				Rooster->KillChicken();
				RemoveSniperFromRooster(Rooster);
			}
		});
		GetWorldTimerManager().SetTimer(RoosterSniper.SniperTimerHandle, SniperDelegate, TimeToSnipe, false);
		PendingRoosterSnipers.Add(RoosterSniper);
	}
}

void AWtcFarm::RemoveSniperFromRooster(AWtcChicken* Rooster)
{
	int32 SniperToRemoveIndex = -1;
	for (int32 Index = 0; Index < PendingRoosterSnipers.Num(); Index++)
	{
		if (Rooster == PendingRoosterSnipers[Index].Rooster)
		{
			FTimerHandle SniperTimer = PendingRoosterSnipers[Index].SniperTimerHandle;
			PendingRoosterSnipers[Index].SniperReticleActor->Destroy();
			GetWorldTimerManager().PauseTimer(SniperTimer);
			SniperToRemoveIndex = Index;
		}
	}
	if (SniperToRemoveIndex >= 0)
	{
		PendingRoosterSnipers.RemoveAt(SniperToRemoveIndex);
	}
}

bool AWtcFarm::HasSniperAttached(AWtcChicken* Rooster)
{
	for (int32 Index = 0; Index < PendingRoosterSnipers.Num(); Index++)
	{
		if (Rooster == PendingRoosterSnipers[Index].Rooster)
		{
			return true;
		}
	}
	return false;
}

void AWtcFarm::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AWtcFarm, Team);
	DOREPLIFETIME(AWtcFarm, MotherHenLife);
	DOREPLIFETIME(AWtcFarm, CurrentAmountOfEggs);
}