// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Components/ActorComponent.h"
#include "WtcAbilityComponent.generated.h"

class AWtcCharacter;
class UWtcMovementComponent;

UCLASS(Blueprintable, ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class WHATTHECLUCK_API UWtcAbilityComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	// Sets default values for this component's properties
	UWtcAbilityComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	void ActivateAbility();
	void DeActivateAbility();

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = "Ability")
		bool PassiveConditional();

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = "Ability")
		void ExecuteAbility();

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = "Ability")
		void ResetAbility();

	UFUNCTION(BlueprintCallable, Category = "General")
		AWtcCharacter* GetOwnerCharacter();

	UFUNCTION(BlueprintCallable, Category = "General")
		UWtcMovementComponent* GetOwnerMovementComponent();

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Stamina")
		float StaminaCost = 0.5f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "General")
		float Duration = 5.0f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "General")
		float Cooldown = 5.0f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "General")
		bool bCanUseAbility = true;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "General")
		bool bIsPassive = false;

private:

	FTimerHandle DurationTimer;
	FTimerDelegate DurationDelegate;

	FTimerHandle CooldownTimer;
	FTimerDelegate CooldownDelegate;
};
