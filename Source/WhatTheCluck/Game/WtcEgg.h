// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "WtcTeam.h"
#include "WtcChicken.h"
#include "WtcEgg.generated.h"

UCLASS()
class WHATTHECLUCK_API AWtcEgg : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AWtcEgg();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Meta = (ExposeOnSpawn = true), Category = "Team")
		EWtcTeam Team;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Chicken")
		TSubclassOf<AWtcChicken> ChickenClass;

};
