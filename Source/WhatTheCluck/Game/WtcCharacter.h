// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/Character.h"
#include "WtcAbilityComponent.h"
#include "WtcBaseAbilityComponent.h"
#include "WtcTeam.h"
#include "WtcCharacter.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FWtcCharacterEvent);

class UWtcAbilityComponent;
class UWtcBaseAbilityComponent;

UCLASS(Blueprintable)
class AWtcCharacter : public ACharacter
{
	GENERATED_BODY()

public:

	AWtcCharacter(const FObjectInitializer& ObjectInitializer);

	virtual void BeginPlay() override;

	// Called every frame.
	virtual void Tick(float DeltaSeconds) override;

	/** Returns TopDownCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetTopDownCameraComponent() const
	{
		return TopDownCameraComponent;
	}
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const
	{
		return CameraBoom;
	}

private:
	/** Top down camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class UCameraComponent* TopDownCameraComponent;

	/** Camera boom positioning the camera above the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class USpringArmComponent* CameraBoom;

public:
	/** Abilities */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Replicated, Category = "Ability", meta = (AllowPrivateAccess = "true"))
		UWtcAbilityComponent* PassiveAbilityOne;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Replicated, Category = "Ability", meta = (AllowPrivateAccess = "true"))
		UWtcAbilityComponent* PassiveAbilityTwo;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Replicated, Category = "Ability", meta = (AllowPrivateAccess = "true"))
		UWtcAbilityComponent* ActiveAbilityOne;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Replicated, Category = "Ability", meta = (AllowPrivateAccess = "true"))
		UWtcAbilityComponent* ActiveAbilityTwo;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Replicated, Category = "Ability", meta = (AllowPrivateAccess = "true"))
		UWtcAbilityComponent* ActiveAbilityThree;

	UPROPERTY(EditAnywhere, Category = "Ability", BlueprintReadWrite)
		TSubclassOf<UWtcAbilityComponent> PassiveAbilityOne_Class;
	UPROPERTY(EditAnywhere, Category = "Ability", BlueprintReadWrite)
		TSubclassOf<UWtcAbilityComponent> PassiveAbilityTwo_Class;
	UPROPERTY(EditAnywhere, Category = "Ability", BlueprintReadWrite)
		TSubclassOf<UWtcAbilityComponent> ActiveAbilityOne_Class;
	UPROPERTY(EditAnywhere, Category = "Ability", BlueprintReadWrite)
		TSubclassOf<UWtcAbilityComponent> ActiveAbilityTwo_Class;
	UPROPERTY(EditAnywhere, Category = "Ability", BlueprintReadWrite)
		TSubclassOf<UWtcAbilityComponent> ActiveAbilityThree_Class;

	/* Base Abilities*/

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "BaseAbility")
		TSubclassOf<UWtcBaseAbilityComponent> PickupAbility_Class;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "BaseAbility")
		TSubclassOf<UWtcBaseAbilityComponent> ThrowingAbility_Class;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "BaseAbility")
		TSubclassOf<UWtcBaseAbilityComponent> TackleAbility_Class;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "BaseAbility")
		TSubclassOf<UWtcBaseAbilityComponent> JumpAbility_Class;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "BaseAbility")
		TSubclassOf<UWtcBaseAbilityComponent> SprintAbility_Class;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Replicated, Category = "BaseAbility")
		UWtcBaseAbilityComponent* PickupAbility;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Replicated, Category = "BaseAbility")
		UWtcBaseAbilityComponent* ThrowingAbility;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Replicated, Category = "BaseAbility")
		UWtcBaseAbilityComponent* TackleAbility;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Replicated, Category = "BaseAbility")
		UWtcBaseAbilityComponent* JumpAbility;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Replicated, Category = "BaseAbility")
		UWtcBaseAbilityComponent* SprintAbility;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Team", Replicated)
		EWtcTeam Team = EWtcTeam::WTC_Red;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Team")
		float OccupyValue = 1.0f;

public:
	/** Events */
	UPROPERTY(BlueprintAssignable, BlueprintCallable, BlueprintReadWrite, Category = "Events")
		FWtcCharacterEvent OnJumpDelegate;
	UPROPERTY(BlueprintAssignable, BlueprintCallable, BlueprintReadWrite, Category = "Events")
		FWtcCharacterEvent OnDoubleJumpDelegate;
	UPROPERTY(BlueprintAssignable, BlueprintCallable, BlueprintReadWrite, Category = "Events")
		FWtcCharacterEvent OnSoftTackleDelegate;
	UPROPERTY(BlueprintAssignable, BlueprintCallable, BlueprintReadWrite, Category = "Events")
		FWtcCharacterEvent OnHardTackleDelegate;
	UPROPERTY(BlueprintAssignable, BlueprintCallable, BlueprintReadWrite, Category = "Events")
		FWtcCharacterEvent OnPickupEggDelegate;
	UPROPERTY(BlueprintAssignable, BlueprintCallable, BlueprintReadWrite, Category = "Events")
		FWtcCharacterEvent OnPickupChickenDelegate;
	UPROPERTY(BlueprintAssignable, BlueprintCallable, BlueprintReadWrite, Category = "Events")
		FWtcCharacterEvent OnDropChickenDelegate;
	UPROPERTY(BlueprintAssignable, BlueprintCallable, BlueprintReadWrite, Category = "Events")
		FWtcCharacterEvent OnCatchDelegate;
	UPROPERTY(BlueprintAssignable, BlueprintCallable, BlueprintReadWrite, Category = "Events")
		FWtcCharacterEvent OnThrowDelegate;
	UPROPERTY(BlueprintAssignable, BlueprintCallable, BlueprintReadWrite, Category = "Events")
		FWtcCharacterEvent OnChargeThrowDelegate;

	UPROPERTY(BlueprintAssignable, BlueprintCallable, BlueprintReadWrite, Category = "Events")
		FWtcCharacterEvent OnStaminaChange;
public:
	/*=============
	STAMINA
	===============*/
	UPROPERTY(VisibleAnywhere, Category = "Stats|Stamina", BlueprintReadOnly, Replicated)
		float Stamina = 1.0f;

	UPROPERTY(EditAnywhere, Category = "Stats|Stamina", BlueprintReadWrite)
		float StaminaSprintingDecay = 0.15f; //Stamina cost per second

	UPROPERTY(EditAnywhere, Category = "Stats|Stamina", BlueprintReadWrite)
		float StaminaRegen = 0.25f; //Stamina recharge per second

	UPROPERTY(EditAnywhere, Category = "Stats|Stamina", BlueprintReadOnly)
		float StaminaRegenSmallDelay = 0.5f; //Delay before recharging

	UPROPERTY(EditAnywhere, Category = "Stats|Stamina", BlueprintReadOnly)
		float StaminaRegenLargeDelay = 2.0f;

	UPROPERTY(VisibleAnywhere, Category = "Stats|Stamina", BlueprintReadOnly)
		bool bCanUseStaminaAbilities = true;

	UPROPERTY(VisibleAnywhere, Category = "Stats|Stamina", BlueprintReadOnly)
		bool bCanRegenerateStamina = true;

	UPROPERTY(EditAnywhere, Category = "Stats|Stamina", BlueprintReadWrite)
		float MinimumStaminaThrowingCost = 0.35f;

	UPROPERTY(EditAnywhere, Category = "Stats|Stamina", BlueprintReadWrite)
		float StaminaThrowingCost = 0.35f;

	UPROPERTY(EditAnywhere, Category = "Stats|Stamina", BlueprintReadWrite)
		float StaminaJumpingCost = 0.2f;

	UPROPERTY(EditAnywhere, Category = "Stats|Stamina", BlueprintReadWrite)
		float StaminaSoftTackleCost = 0.25f;

	UPROPERTY(EditAnywhere, Category = "Stats|Stamina", BlueprintReadWrite)
		float StaminaHardTackleCost = 0.75f;

	/***********************************************************************
	 Removes a certain percentage of Stamina, range from 0.0 to 1.0
	************************************************************************/
	UFUNCTION(BlueprintCallable, Category = "Stats|Stamina")
		bool RemoveStamina(float ValuePercent);

	/***********************************************************************
	 Adds a certain percentage of Stamina, range from 0.0 to 1.0
	************************************************************************/
	UFUNCTION(BlueprintCallable, Category = "Stats|Stamina")
		bool AddStamina(float ValuePercent);

private:
	FTimerHandle StaminaCooldownTimer;
	FTimerDelegate StaminaCooldownDelegate;

	void RegenerateStamina(float DeltaSeconds);
	void ActivateSmallStaminaCooldown();
	void ActivateLargeStaminaCooldown();

public:

	/*=============
	PICKUP
	===============*/
	UPROPERTY(EditAnywhere, Category = "Stats|Pickup", BlueprintReadWrite)
		float PickupHitboxWidth = 200.0f; //Rectangular hitbox
	UPROPERTY(EditAnywhere, Category = "Stats|Pickup", BlueprintReadWrite)
		float PickupHitboxHeight = 200.0f; //Rectangular hitbox

	/*=============
	CATCHING
	===============*/
	UPROPERTY(EditAnywhere, Category = "Stats|Catching", BlueprintReadWrite)
		float CatchingHitboxRadius = 250.0f;

	/*=============
	THROWING
	===============*/
	UPROPERTY(EditAnywhere, Category = "Stats|Throwing", BlueprintReadWrite)
		float MinThrowingRange = 850.0f;
	UPROPERTY(EditAnywhere, Category = "Stats|Throwing", BlueprintReadWrite)
		float MaxThrowingRange = 4000.0f;
	UPROPERTY(EditAnywhere, Category = "Stats|Throwing", BlueprintReadWrite)
		float ThrowingChargeTime = 1.5f; //in seconds 
	UPROPERTY(EditAnywhere, Category = "Stats|Throwing", BlueprintReadWrite)
		float ThrowingArchHeight = 900.0f;
	UPROPERTY(EditAnywhere, Category = "Stats|Throwing", BlueprintReadWrite)
		float ChargeTreshold = 0.2f;

	/*=============
	MOVEMENT
	===============*/
	UPROPERTY(EditDefaultsOnly, Category = "Stats|Movement", BlueprintReadOnly)
		float DefaultWalkingSpeed = 600.0f;
	UPROPERTY(EditAnywhere, Category = "Stats|Movement", BlueprintReadWrite, Replicated)
		float CurrentMovementSpeed = 600.0f;
	UPROPERTY(EditAnywhere, Category = "Stats|Movement", BlueprintReadWrite)
		float MovementPenalty = 0.0f;
	UPROPERTY(EditAnywhere, Category = "Stats|Movement", BlueprintReadWrite)
		bool  DynamicMovementPenalty = false;
	UPROPERTY(EditAnywhere, Category = "Stats|Movement", BlueprintReadWrite)
		float ExhaustionModifier = 0.5f;
	UPROPERTY(EditDefaultsOnly, Category = "Stats|Movement", BlueprintReadOnly)
		float DefaultSprintSpeed = 1200.0f;
	UPROPERTY(EditAnywhere, Category = "Stats|Movement", BlueprintReadWrite)
		float JumpHeight = 100.0f;
	UPROPERTY(EditAnywhere, Category = "Stats|Movement", BlueprintReadWrite)
		float DoubleJumpHeight = 100.0f;
	UPROPERTY(EditAnywhere, Category = "Stats|Movement", BlueprintReadWrite)
		float GroundFrictionDefault = 8.0f;

	/*=============
	ATTACKING
	===============*/
	UPROPERTY(EditAnywhere, Category = "Stats|Attacking", BlueprintReadWrite)
		float SoftTackleHitboxRadius;
	UPROPERTY(EditAnywhere, Category = "Stats|Attacking", BlueprintReadWrite)
		float SoftTackleHitboxWidth;
	UPROPERTY(EditAnywhere, Category = "Stats|Attacking", BlueprintReadWrite)
		float SoftTackleHitboxHeight;
	UPROPERTY(EditAnywhere, Category = "Stats|Attacking", BlueprintReadWrite)
		float SoftTackleCooldown = 0.75f;
	UPROPERTY(EditAnywhere, Category = "Stats|Attacking", BlueprintReadWrite)
		float SoftTackleDistance = 1500.0f;
	UPROPERTY(EditAnywhere, Category = "Stats|Attacking", BlueprintReadWrite)
		float HardTackleHitboxRadius;
	UPROPERTY(EditAnywhere, Category = "Stats|Attacking", BlueprintReadWrite)
		float HardTackleHitboxWidth;
	UPROPERTY(EditAnywhere, Category = "Stats|Attacking", BlueprintReadWrite)
		float HardTackleHitboxHeight;
	UPROPERTY(EditAnywhere, Category = "Stats|Attacking", BlueprintReadWrite)
		float HardTackleCooldown = 1.5f;
	UPROPERTY(EditAnywhere, Category = "Stats|Attacking", BlueprintReadWrite)
		float HardTackleKnockbackDistance = 1000.0f;
	UPROPERTY(EditAnywhere, Category = "Stats|Attacking", BlueprintReadWrite)
		float HardTackleTackleChargeTime = 1.0f;
	UPROPERTY(EditAnywhere, Category = "Stats|Attacking", BlueprintReadWrite)
		float HardTackleDistance = 2000.0f;
	UPROPERTY(EditAnywhere, Category = "Stats|Attacking", BlueprintReadWrite)
		bool bCanBeTackled = true;

	/*=============
	ABILITIES
	===============*/
	UPROPERTY(EditAnywhere, Category = "Abilities", BlueprintReadWrite, Replicated)
		bool CanSprint = false;
	UPROPERTY(EditAnywhere, Category = "Abilities", BlueprintReadWrite, Replicated)
		bool IsSprinting = false;
	UPROPERTY(EditAnywhere, Category = "Abilities", BlueprintReadWrite, Replicated)
		bool bJumpPressed = false;
	UPROPERTY(EditAnywhere, Category = "Abilities", BlueprintReadWrite)
		bool CanDoubleJump = true;
	UPROPERTY(EditAnywhere, Category = "Abilities", BlueprintReadWrite)
		bool CanDoubleJumpWithChicken = false;
	UPROPERTY(EditAnywhere, Category = "Abilities", BlueprintReadWrite)
		bool IgnoreCarryPenalties = false;
};
