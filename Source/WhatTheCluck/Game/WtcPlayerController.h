// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/PlayerController.h"
#include "WtcTeam.h"
#include "UMG.h"
#include "WtcPlayerController.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FWtcGameEvent);

/*
The PlayerController exists on each client per player on that machine.
Replicated between the server and the associated client, but are not replicated to other clients.
Well-suited to communicating information between clients and servers without replicating this information to other clients.
*/

class AWtcCharacter;

UCLASS()
class AWtcPlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	AWtcPlayerController();

	UPROPERTY(EditAnywhere, Category = "Team", BlueprintReadWrite, Replicated)
		EWtcTeam Team;

	UFUNCTION(BlueprintCallable, Category = "General")
		AWtcCharacter* GetCharacterPawn();

	UPROPERTY(BlueprintAssignable, BlueprintCallable, BlueprintReadWrite, Category = "General")
		FWtcGameEvent OnPossessPawn;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "HUD")
		TSubclassOf<UUserWidget> HUDWidgetClass;

	UUserWidget* HUDWidget;

private:

	bool IsMovementAllowed() const;

	virtual void SetPawn(APawn* InPawn) override;

protected:
	/** True if the controlled character should navigate to the mouse cursor. */
	uint32 bMoveToMouseCursor : 1;

	// Begin PlayerController interface
	virtual void PlayerTick(float DeltaTime) override;
	virtual void SetupInputComponent() override;
	// End PlayerController interface

	void MoveForward(float Value);
	void MoveRight(float Value);

	void AimHorizontal(float Value);
	void AimVertical(float Value);

	void ActivateAbilityOne_Pressed();
	void ActivateAbilityTwo_Pressed();
	void ActivateAbilityThree_Pressed();

	void ActivateAbilityOne_Released();
	void ActivateAbilityTwo_Released();
	void ActivateAbilityThree_Released();

	UPROPERTY(BlueprintAssignable, BlueprintReadWrite, Category = "Movement")
		FWtcGameEvent OnJumpPressedDelegate;
	UPROPERTY(BlueprintAssignable, BlueprintReadWrite, Category = "Movement")
		FWtcGameEvent OnJumpReleasedDelegate;
	UPROPERTY(BlueprintAssignable, BlueprintReadWrite, Category = "Movement")
		FWtcGameEvent OnSprintPressedDelegate;
	UPROPERTY(BlueprintAssignable, BlueprintReadWrite, Category = "Movement")
		FWtcGameEvent OnSprintReleasedDelegate;

	void ThrowPressed();
	void ThrowReleased();
	void InteractPressed();
	void InteractReleased();
	void JumpPressed();
	void JumpReleased();
	void SprintPressed();
	void SprintReleased();
	void TacklePressed();
	void TackleReleased();

	UFUNCTION(BlueprintImplementableEvent, Category = "Abilities")
		void BP_InteractPressed();
	UFUNCTION(BlueprintImplementableEvent, Category = "Abilities")
		void BP_InteractReleased();

	UFUNCTION(BlueprintImplementableEvent, Category = "Abilities")
		void BP_ThrowPressed();
	UFUNCTION(BlueprintImplementableEvent, Category = "Abilities")
		void BP_ThrowReleased();

	UFUNCTION(BlueprintImplementableEvent, Category = "Abilities")
		void BP_TacklePressed();
	UFUNCTION(BlueprintImplementableEvent, Category = "Abilities")
		void BP_TackleReleased();

	UFUNCTION(BlueprintImplementableEvent, Category = "Movement")
		void BP_JumpPressed();
	UFUNCTION(BlueprintImplementableEvent, Category = "Movement")
		void BP_JumpReleased();

	UFUNCTION(BlueprintImplementableEvent, Category = "Movement")
		void BP_SprintPressed();
	UFUNCTION(BlueprintImplementableEvent, Category = "Movement")
		void BP_SprintReleased();

public:
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		TSubclassOf<UUserWidget> CountdownWidgetClass;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		TSubclassOf<UUserWidget> FarmBarnHUDClass;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		TSubclassOf<UUserWidget> EndScreenWidgetClass;

	UFUNCTION(Client, Reliable)
		void StartCountdown();

	UFUNCTION(Client, Reliable)
		void StartPlaying();

	UFUNCTION(Client, Reliable)
		void ShowEndscreen();
};
