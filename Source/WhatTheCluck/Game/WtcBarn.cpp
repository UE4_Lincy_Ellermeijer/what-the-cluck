// Fill out your copyright notice in the Description page of Project Settings.

#include "WhatTheCluck.h"
#include "WtcBarn.h"
#include "UnrealNetwork.h"


// Sets default values
AWtcBarn::AWtcBarn()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	Team = EWtcTeam::WTC_Neutral;
}

// Called when the game starts or when spawned
void AWtcBarn::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AWtcBarn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}


void AWtcBarn::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	//Physics Gravity
	DOREPLIFETIME(AWtcBarn, Team);
	DOREPLIFETIME(AWtcBarn, CurrentEggAmount);
	DOREPLIFETIME(AWtcBarn, bIsLocked);
	DOREPLIFETIME(AWtcBarn, CurrentRedTeamCaptureProgress);
	DOREPLIFETIME(AWtcBarn, CurrentBlueTeamCaptureProgress);
	DOREPLIFETIME(AWtcBarn, BarnOccupyCapValue);
}