// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/PlayerController.h"
#include "WtcMenuPlayerController.generated.h"

/**
 *
 */
UCLASS()
class WHATTHECLUCK_API AWtcMenuPlayerController : public APlayerController
{
	GENERATED_BODY()
public:

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "General")
		bool IsActive = false;
};
