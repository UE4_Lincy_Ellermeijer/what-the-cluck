// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "WtcTeam.h"
#include "GameFramework/Actor.h"
#include "WtcBarn.generated.h"

UENUM(BlueprintType)
enum class EWtcBarnSize : uint8
{
	WTC_Small UMETA(DisplayName = "Small"),
	WTC_Middle UMETA(DisplayName = "Middle"),
	WTC_Large UMETA(DisplayName = "Large"),
	WTC_None UMETA(DisplayName = "None")
};

UCLASS()
class WHATTHECLUCK_API AWtcBarn : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AWtcBarn();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Replicated, Category = "Team")
		EWtcTeam Team;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "General")
		EWtcBarnSize BarnSize = EWtcBarnSize::WTC_Small;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Team")
		float EggRespawnTime = 2.0f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Egg")
		int32 MaximumEggAmount = 6;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Egg")
		TSubclassOf<AActor> EggClass;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Depletion")
		float DepletionPercentageValue = 0.2f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Depletion")
		float DepletionInterval = 5.0f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Depletion")
		float DepletionInitialDelay = 10.0f;

private:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Replicated, Category = "Capture", meta = (AllowPrivateAccess = "true"))
		float CurrentRedTeamCaptureProgress = 0.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Replicated, Category = "Capture", meta = (AllowPrivateAccess = "true"))
		float CurrentBlueTeamCaptureProgress = 0.0f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Replicated, Category = "Capture", meta = (AllowPrivateAccess = "true"))
		float BarnOccupyCapValue = 1.0f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Replicated, Category = "Capture", meta = (AllowPrivateAccess = "true"))
		float LockedTime = 15.0f;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Replicated, Category = "Capture", meta = (AllowPrivateAccess = "true"))
		bool bIsLocked = false;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Replicated, Category = "Egg", meta = (AllowPrivateAccess = "true"))
		int32 CurrentEggAmount = 0;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Depletion", meta = (AllowPrivateAccess = "true"))
		FTimerHandle BlueTeamDepletionHandle;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Depletion", meta = (AllowPrivateAccess = "true"))
		FTimerHandle RedTeamDepletionHandle;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Depletion", meta = (AllowPrivateAccess = "true"))
		FTimerHandle BlueTeamDepletionInitialDelayHandle;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Depletion", meta = (AllowPrivateAccess = "true"))
		FTimerHandle RedTeamDepletionInitialDelayHandle;
};
