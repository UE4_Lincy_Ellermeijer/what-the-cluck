// Fill out your copyright notice in the Description page of Project Settings.

#include "WhatTheCluck.h"
#include "WtcCharacter.h"
#include "UnrealNetwork.h"
#include "WtcBaseAbilityComponent.h"

UWtcBaseAbilityComponent::UWtcBaseAbilityComponent()
{
	PrimaryComponentTick.bCanEverTick = true;
	bReplicates = true;
}

void UWtcBaseAbilityComponent::BeginPlay()
{
	Super::BeginPlay();
}

void UWtcBaseAbilityComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
}

AWtcCharacter* UWtcBaseAbilityComponent::GetOwnerCharacter()
{
	return Cast<AWtcCharacter>(GetOwner());
}

void UWtcBaseAbilityComponent::AbilityButtonPressed()
{
	OnTryActivateAbility.Broadcast();
	bool IsUsable = CanUseAbility();
	if (IsUsable)
	{
		ActivateAbility();
		OnActivateAbility.Broadcast();
	}
}

void UWtcBaseAbilityComponent::AbilityButtonReleased()
{
	DeActivateAbility();
	OnDeActivateAbility.Broadcast();
}

void UWtcBaseAbilityComponent::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(UWtcBaseAbilityComponent, OnTryActivateAbility);
	DOREPLIFETIME(UWtcBaseAbilityComponent, OnActivateAbility);
	DOREPLIFETIME(UWtcBaseAbilityComponent, OnDeActivateAbility);
	DOREPLIFETIME(UWtcBaseAbilityComponent, OnSuccessfulActivation);
	DOREPLIFETIME(UWtcBaseAbilityComponent, IsEnabled);

}
