// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/PlayerState.h"
#include "WtcPlayerState.generated.h"

/*
A PlayerState will exist for every player connected to the game on both the server and the clients. 
This class can be used for replicated properties that all clients, not just the owning client, are interested in, such as the individual player's current score.
*/
UCLASS()
class WHATTHECLUCK_API AWtcPlayerState : public APlayerState
{
	GENERATED_UCLASS_BODY()
	
public:
	
	
};
