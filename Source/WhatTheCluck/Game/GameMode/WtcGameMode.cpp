// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "WhatTheCluck.h"
#include "WtcGameMode.h"
#include "WtcPlayerController.h"
#include "WtcCharacter.h"
#include "WtcPlayerstart.h"
#include "WtcGameState.h"
#include "WtcTeam.h"
#include "WtcGameInstance.h"
#include "WtcFarm.h"
#include "WtcUtility.h"

AWtcGameMode::AWtcGameMode()
{
	PlayerControllerClass = AWtcPlayerController::StaticClass();
	DefaultPawnClass = AWtcCharacter::StaticClass();
	PrimaryActorTick.bCanEverTick = true;
	SetupStateMachine();
	StateMachine->NotifyImmediate(EWtcGameModeEvent::InitialEnter);
}

void AWtcGameMode::EndGame(EWtcTeam WinningTeam)
{
	GetWtcGameState()->WinningTeam = WinningTeam;
	StateMachine->NotifyImmediate(EWtcGameModeEvent::GameFinished);
}

void AWtcGameMode::BeginPlay()
{
	Super::BeginPlay();
	if (WtcGameSize != EWtcGameSize::WTC_Solo)
	{
		TArray<AActor*> FoundFarms;
		UGameplayStatics::GetAllActorsOfClass(GetWorld(), AWtcFarm::StaticClass(), FoundFarms);
		check(FoundFarms.Num() == 2);
		for (int32 Index = 0; Index < FoundFarms.Num(); Index++)
		{
			AWtcFarm* Farm = Cast<AWtcFarm>(FoundFarms[Index]);
			if (Farm->Team == EWtcTeam::WTC_Red)
			{
				RedTeamFarm = Farm;
			}
			else if (Farm->Team == EWtcTeam::WTC_Blue)
			{
				BlueTeamFarm = Farm;
			}
		}
	}
	GetWtcGameState()->CountdownDuration = CountdownDuration;
	GetWtcGameState()->EndScreenDuration = EndScreenDuration;
	GetWtcGameState()->GameSize = WtcGameSize;
}

void AWtcGameMode::InitGame(const FString & MapName, const FString & Options, FString & ErrorMessage)
{
	Super::InitGame(MapName, Options, ErrorMessage);
	UWhatTheCluckGameInstance* GameInstance = Cast<UWhatTheCluckGameInstance>(GetGameInstance());
	if (GameInstance)
	{
		WtcGameSize = GameInstance->GameSize;
	}

	TArray<AActor*> FoundActors;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), AWtcPlayerStart::StaticClass(), FoundActors);

	for (AActor* APlayerStart : FoundActors)
	{
		AWtcPlayerStart* WtcPlayerStart = (AWtcPlayerStart*)APlayerStart;

		switch (WtcPlayerStart->Team)
		{
		case EWtcTeam::WTC_Blue:
			BlueTeamSpawnPoints.Add(WtcPlayerStart);
			break;
		case EWtcTeam::WTC_Red:
			RedTeamSpawnPoints.Add(WtcPlayerStart);
			break;
		default:
			break;
		}
	}
}

APlayerController * AWtcGameMode::SpawnPlayerController(ENetRole InRemoteRole, FVector const & SpawnLocation, FRotator const & SpawnRotation)
{
	AWtcPlayerController* WtcPlayer = Cast<AWtcPlayerController>(Super::SpawnPlayerController(InRemoteRole, SpawnLocation, SpawnRotation));

	int32 PlayersNeededPerTeam = GetNeededPlayerCountPerTeam();

	if (WtcPlayer->Team == EWtcTeam::WTC_Red && CurrentPlayerAmountTeamRed < PlayersNeededPerTeam)
	{
		++CurrentPlayerAmountTeamRed;
	}
	else if (WtcPlayer->Team == EWtcTeam::WTC_Blue && CurrentPlayerAmountTeamBlue < PlayersNeededPerTeam)
	{
		++CurrentPlayerAmountTeamBlue;
	}
	else
	{
		if (CurrentPlayerAmountTeamRed < PlayersNeededPerTeam)
		{
			WtcPlayer->Team = EWtcTeam::WTC_Red;
			++CurrentPlayerAmountTeamRed;
		}
		else if (CurrentPlayerAmountTeamBlue < PlayersNeededPerTeam)
		{
			WtcPlayer->Team = EWtcTeam::WTC_Blue;
			++CurrentPlayerAmountTeamBlue;
		}
	}

	return WtcPlayer;
}

void AWtcGameMode::PostLogin(APlayerController* NewPlayer)
{
	Super::PostLogin(NewPlayer);
}

AWtcGameState* AWtcGameMode::GetWtcGameState()
{
	return Cast<AWtcGameState>(GameState);
}

int32 AWtcGameMode::GetNeededPlayerCountPerTeam()
{
	switch (WtcGameSize)
	{
	case EWtcGameSize::WTC_OneVsOne:
		return 1;
	case EWtcGameSize::WTC_TwoVsTwo:
		return 2;
	case EWtcGameSize::WTC_ThreeVsThree:
		return 3;
	case EWtcGameSize::WTC_FourVsFour:
		return 4;
	case EWtcGameSize::WTC_Solo:
		return 0;
	default:
		return 8;
	}
}

/** Choose the PlayerStart according to the team that is set on the PlayerController */
AActor* AWtcGameMode::ChoosePlayerStart_Implementation(AController * Player)
{
	AActor* DefaultSpawn = Super::ChoosePlayerStart_Implementation(Player);
	AWtcPlayerController* WtcPlayer = (AWtcPlayerController*)Player;

	if (WtcPlayer->Team == EWtcTeam::WTC_Blue && BlueTeamSpawnPoints.Num() > 0)
	{
		int32 Min = 0;
		int32 Max = BlueTeamSpawnPoints.Num() - 1;
		int32 RandomSpawnIndex = FMath::RandRange(Min, Max);
		return BlueTeamSpawnPoints[RandomSpawnIndex];
	}
	else if (WtcPlayer->Team == EWtcTeam::WTC_Red && RedTeamSpawnPoints.Num() > 0)
	{
		int32 Min = 0;
		int32 Max = RedTeamSpawnPoints.Num() - 1;
		int32 RandomSpawnIndex = FMath::RandRange(Min, Max);
		return RedTeamSpawnPoints[RandomSpawnIndex];
	}
	return DefaultSpawn;
}

void AWtcGameMode::RestartPlayer(class AController* NewPlayer)
{
	Super::RestartPlayer(NewPlayer);
}

void AWtcGameMode::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);
	StateMachine->Update(DeltaSeconds);
	PrintCurrentState(0.0f);
}

void AWtcGameMode::OnEnterPlaying(EWtcGameModeState State, EWtcGameModeEvent Event)
{
	AWtcGameState* WtcGameState = GetWtcGameState();
	WtcGameState->bIsInPlayingState = true;
	UWorld* World = GetWorld();
	for (FConstPlayerControllerIterator Iterator = World->GetPlayerControllerIterator(); Iterator; ++Iterator)
	{
		AWtcPlayerController* PlayerController = Cast<AWtcPlayerController>(Iterator->Get());
		if (PlayerController)
		{
			PlayerController->StartPlaying();
		}
	}
}

void AWtcGameMode::OnEnterCountdown(EWtcGameModeState State, EWtcGameModeEvent Event)
{
	UWorld* World = GetWorld();
	for (FConstPlayerControllerIterator Iterator = World->GetPlayerControllerIterator(); Iterator; ++Iterator)
	{
		AWtcPlayerController* PlayerController = Cast<AWtcPlayerController>(Iterator->Get());
		if (PlayerController)
		{
			PlayerController->StartCountdown();
		}
	}
}

void AWtcGameMode::OnEnterWaitingForPlayers(EWtcGameModeState State, EWtcGameModeEvent Event)
{
	CurrentPlayerAmountTeamRed = 0.0f;
	CurrentPlayerAmountTeamBlue = 0.0f;
}

void AWtcGameMode::OnEnterFinished(EWtcGameModeState State, EWtcGameModeEvent Event)
{
	GetWtcGameState()->bIsGameFinished = true;
	GetWtcGameState()->bIsInPlayingState = false;
	UWorld* World = GetWorld();
	for (FConstPlayerControllerIterator Iterator = World->GetPlayerControllerIterator(); Iterator; ++Iterator)
	{
		AWtcPlayerController* PlayerController = Cast<AWtcPlayerController>(Iterator->Get());
		if (PlayerController)
		{
			PlayerController->ShowEndscreen();
		}
	}
}


void AWtcGameMode::OnLeaveFinished(EWtcGameModeState State, EWtcGameModeEvent Event)
{
	FName MainMenu("MainMenu");
	UGameplayStatics::OpenLevel(GetWorld(), MainMenu);
}

void AWtcGameMode::UpdateWaitingForPlayers(float DeltaTime)
{
	int32 PlayersNeeded = GetNeededPlayerCountPerTeam() * 2;
	int32 CurrentPlayers = CurrentPlayerAmountTeamBlue + CurrentPlayerAmountTeamRed;
	if (PlayersNeeded == CurrentPlayers || WtcGameSize == EWtcGameSize::WTC_Solo)
	{
		StateMachine->NotifyImmediate(EWtcGameModeEvent::AllPlayersReady);
	}
}

void AWtcGameMode::UpdateCountdown(float DeltaTime)
{
	if (WtcGameSize == EWtcGameSize::WTC_Solo)
	{
		StateMachine->NotifyImmediate(EWtcGameModeEvent::CountdownFinished);
	}
	AWtcGameState* WtcGameState = GetWtcGameState();
	WtcGameState->CountdownTimer += DeltaTime;
	if (WtcGameState->CountdownTimer >= (CountdownDuration + 1))
	{
		StateMachine->NotifyImmediate(EWtcGameModeEvent::CountdownFinished);
	}
}

void AWtcGameMode::UpdateFinished(float DeltaTime)
{
	AWtcGameState* WtcGameState = GetWtcGameState();
	WtcGameState->EndScreenTimer += DeltaTime;
	if (WtcGameState->EndScreenTimer >= EndScreenDuration)
	{
		StateMachine->NotifyImmediate(EWtcGameModeEvent::RestartGame);
	}
}

void AWtcGameMode::SetupStateMachine()
{
	StateMachine.Reset(new FWtcGameModeStateMachine(EWtcGameModeState::Initial, EWtcGameModeEvent::InitialEnter));

	StateMachine->State(EWtcGameModeState::Initial)
		.Transition(EWtcGameModeState::WaitingForPlayers, EWtcGameModeEvent::InitialEnter)
		;

	StateMachine->State(EWtcGameModeState::WaitingForPlayers)
		.Transition(EWtcGameModeState::Countdown, EWtcGameModeEvent::AllPlayersReady)
		.OnEnter(BIND_UOBJECT_DELEGATE(FWtcGameModeStateMachine::OnEnterStateDelegate, OnEnterWaitingForPlayers))
		.OnUpdate(BIND_UOBJECT_DELEGATE(FWtcGameModeStateMachine::OnUpdateStateDelegate, UpdateWaitingForPlayers))
		;

	StateMachine->State(EWtcGameModeState::Countdown)
		.Transition(EWtcGameModeState::Playing, EWtcGameModeEvent::CountdownFinished)
		.OnEnter(BIND_UOBJECT_DELEGATE(FWtcGameModeStateMachine::OnEnterStateDelegate, OnEnterCountdown))
		.OnUpdate(BIND_UOBJECT_DELEGATE(FWtcGameModeStateMachine::OnUpdateStateDelegate, UpdateCountdown))
		;

	StateMachine->State(EWtcGameModeState::Playing)
		.Transition(EWtcGameModeState::Finished, EWtcGameModeEvent::GameFinished)
		.Transition(EWtcGameModeState::Paused, EWtcGameModeEvent::PausePressed)
		.OnEnter(BIND_UOBJECT_DELEGATE(FWtcGameModeStateMachine::OnEnterStateDelegate, OnEnterPlaying))
		;

	StateMachine->State(EWtcGameModeState::Paused)
		.Transition(EWtcGameModeState::Playing, EWtcGameModeEvent::PausePressed)
		;

	StateMachine->State(EWtcGameModeState::Finished)
		.Transition(EWtcGameModeState::Countdown, EWtcGameModeEvent::RestartGame)
		.OnEnter(BIND_UOBJECT_DELEGATE(FWtcGameModeStateMachine::OnEnterStateDelegate, OnEnterFinished))
		.OnUpdate(BIND_UOBJECT_DELEGATE(FWtcGameModeStateMachine::OnUpdateStateDelegate, UpdateFinished))
		.OnLeave(BIND_UOBJECT_DELEGATE(FWtcGameModeStateMachine::OnLeaveStateDelegate, OnLeaveFinished))
		;

	StateMachine->Update(0.0f);
}
