// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/GameMode.h"
#include "WtcPlayerStart.h"
#include "WtcTeam.h"
#include "WtcStateMachine.h"
#include "WtcGameSize.h"
#include "WtcGameMode.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FWtcGameModeEvent);

class AWtcFarm;
class AWtcGameState;

/*
The GameMode only exists on the server. It generally stores information related to the game that clients do not need to know explicitly.
*/
UENUM(BlueprintType)
enum EWtcGameModeState
{
	Initial,
	WaitingForPlayers,
	Countdown,
	Playing,
	Paused,
	Finished,
};

UENUM(BlueprintType)
enum EWtcGameModeEvent
{
	InitialEnter,
	CountdownFinished,
	AllPlayersReady,
	GameFinished,
	PausePressed,
	RestartGame,
};

typedef TWtcStateMachine<EWtcGameModeState, EWtcGameModeEvent> FWtcGameModeStateMachine;


UCLASS(Abstract)
class AWtcGameMode : public AGameMode
{
	GENERATED_BODY()

public:
	AWtcGameMode();



	UFUNCTION(BlueprintCallable, Category = "Score")
		void EndGame(EWtcTeam WinningTeam);
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "GameSize")
		EWtcGameSize WtcGameSize;
private:

	virtual void BeginPlay() override;
	virtual AActor * ChoosePlayerStart_Implementation(AController * Player) override;
	virtual void RestartPlayer(class AController* NewPlayer) override;
	virtual void InitGame(const FString & MapName, const FString & Options, FString & ErrorMessage) override;
	virtual void Tick(float DeltaSeconds) override;
	virtual APlayerController * SpawnPlayerController(ENetRole InRemoteRole, FVector const & SpawnLocation, FRotator const & SpawnRotation) override;
	virtual void PostLogin(APlayerController * NewPlayer);

	AWtcGameState* GetWtcGameState();



	EWtcTeam LastTeam;
	int CurrentPlayerAmountTeamRed = 0;
	int CurrentPlayerAmountTeamBlue = 0;

	TArray<AWtcPlayerStart*> RedTeamSpawnPoints;
	TArray<AWtcPlayerStart*> BlueTeamSpawnPoints;

	UPROPERTY()
		AWtcFarm* BlueTeamFarm;

	UPROPERTY()
		AWtcFarm* RedTeamFarm;

	int32 GetNeededPlayerCountPerTeam();

	/**
		State machine start
	*/
public:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "GameState|Counters")
		float CountdownDuration = 10.0f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "GameState|Counters")
		float GameDuration = 300.0f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "GameState|Counters")
		float EndScreenDuration = 30.0f;

private:

	void OnEnterPlaying(EWtcGameModeState State, EWtcGameModeEvent Event);
	void OnEnterCountdown(EWtcGameModeState State, EWtcGameModeEvent Event);
	void OnEnterWaitingForPlayers(EWtcGameModeState State, EWtcGameModeEvent Event);
	void OnEnterFinished(EWtcGameModeState State, EWtcGameModeEvent Event);

	void OnLeaveFinished(EWtcGameModeState State, EWtcGameModeEvent Event);

	void UpdateWaitingForPlayers(float DeltaTime);
	void UpdateCountdown(float DeltaTime);
	void UpdateFinished(float DeltaTime);

	void SetupStateMachine();
	TUniquePtr<FWtcGameModeStateMachine> StateMachine;

	void PrintCurrentState(float Time)
	{
		FString OrigName = TO_STRING(EWtcGameModeState);
		UEnum* Enum = FindObject<UEnum>(ANY_PACKAGE, *OrigName, true);
		ensureMsgf(Enum, TEXT("Could not find enum '%s'"), *OrigName);
		FString Name;
		if (Enum != nullptr)
		{
			Name = Enum->GetEnumNameStringByValue(static_cast<int32>(StateMachine->CurrentState));
		}
		if (GEngine)
		{
			GEngine->AddOnScreenDebugMessage(-1, Time, FColor::Red, FString::Printf(TEXT("Current State: %s"), *Name));
		}
	}

	/**
		State machine end
	*/
};



