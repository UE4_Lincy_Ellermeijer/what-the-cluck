// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/PlayerStart.h"
#include "WtcTeam.h"
#include "WtcPlayerStart.generated.h"


/**
 *
 */
UCLASS(BlueprintType)
class WHATTHECLUCK_API AWtcPlayerStart : public APlayerStart
{
	GENERATED_BODY()

public:

	AWtcPlayerStart();

	/* The team that this spawn is for */
	UPROPERTY(EditAnywhere, Category = "Team", BlueprintReadWrite)
		EWtcTeam Team;
};
