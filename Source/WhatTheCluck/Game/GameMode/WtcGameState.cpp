// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "WhatTheCluck.h"
#include "WtcGameState.h"
#include "WtcCharacter.h"
#include "WtcPlayerController.h"
#include "UnrealNetwork.h"

AWtcGameState::AWtcGameState(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	bAllPlayersPresent = false;
	bIsInPlayingState = false;
}

void AWtcGameState::HandleBeginPlay()
{
	bReplicatedHasBegunPlay = true;

	GetWorldSettings()->NotifyBeginPlay();
	GetWorldSettings()->NotifyMatchStarted();
}

bool AWtcGameState::HasBegunPlay() const
{
	UWorld* World = GetWorld();
	if (World)
	{
		return World->bBegunPlay;
	}

	return false;
}

void AWtcGameState::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AWtcGameState, bAllPlayersPresent);
	DOREPLIFETIME(AWtcGameState, bIsInPlayingState);
	DOREPLIFETIME(AWtcGameState, EndScreenTimer);
	DOREPLIFETIME(AWtcGameState, CountdownTimer);
	DOREPLIFETIME(AWtcGameState, EndScreenDuration);
	DOREPLIFETIME(AWtcGameState, CountdownDuration);
	DOREPLIFETIME(AWtcGameState, bCountdownActive);
	DOREPLIFETIME(AWtcGameState, bIsGameFinished);
	DOREPLIFETIME(AWtcGameState, GameSize);
	DOREPLIFETIME(AWtcGameState, WinningTeam);
	DOREPLIFETIME(AWtcGameState, BlueStatistics);
	DOREPLIFETIME(AWtcGameState, RedStatistics);
}

void AWtcGameState::IncrementPickedUpEggs(EWtcTeam Team)
{
	if (Team == EWtcTeam::WTC_Red)
	{
		++RedStatistics.PickedUpEggs;
	}
	else
	{
		++BlueStatistics.PickedUpEggs;
	}
}

void AWtcGameState::IncrementGotTackled(EWtcTeam Team)
{
	if (Team == EWtcTeam::WTC_Red)
	{
		++RedStatistics.GotTackled;
	}
	else
	{
		++BlueStatistics.GotTackled;
	}
}

void AWtcGameState::IncrementHasTackled(EWtcTeam Team)
{
	if (Team == EWtcTeam::WTC_Red)
	{
		++RedStatistics.HasTackled;
	}
	else
	{
		++BlueStatistics.HasTackled;
	}
}

void AWtcGameState::IncrementThrownEggs(EWtcTeam Team)
{
	if (Team == EWtcTeam::WTC_Red)
	{
		++RedStatistics.ThrownEggs;
	}
	else
	{
		++BlueStatistics.ThrownEggs;
	}
}
