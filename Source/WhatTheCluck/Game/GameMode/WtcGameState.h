// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#pragma once
#include "GameFramework/GameState.h"
#include "WtcTeam.h"
#include "WtcCharacter.h"
#include "WtcTeamStatistics.h"
#include "WtcGameSize.h"
#include "WtcGameState.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FWtcGameStateEvent);

/*
The GameState exists on the server and the clients.
Use replicated variables on the GameState to keep all clients up-to-date on data about the game.
Information that is of interest to all players and spectators, but isn't associated with any one specific player, is ideal for GameState replication.
*/
UCLASS()
class WHATTHECLUCK_API AWtcGameState : public AGameState
{
	GENERATED_UCLASS_BODY()



public:
	/** Returns true if the world has started play (called BeginPlay on actors) */
	UFUNCTION(BlueprintCallable, Category = GameState)
		virtual bool HasBegunPlay() const;


	/** Called by game mode to set the started play bool */
	virtual void HandleBeginPlay();

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Replicated, Category = "General")
		EWtcGameSize GameSize = EWtcGameSize::WTC_Solo;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Replicated, Category = "Start Game")
		bool bAllPlayersPresent = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Replicated, Category = "Start Game")
		bool bIsInPlayingState = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Replicated, Category = "End Game")
		EWtcTeam WinningTeam;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Replicated, Category = "End Game")
		FWtcTeamStatistics BlueStatistics;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Replicated, Category = "End Game")
		FWtcTeamStatistics RedStatistics;

	UFUNCTION(BlueprintCallable, Category = "Statistics")
		void IncrementPickedUpEggs(EWtcTeam Team);

	UFUNCTION(BlueprintCallable, Category = "Statistics")
		void IncrementGotTackled(EWtcTeam Team);

	UFUNCTION(BlueprintCallable, Category = "Statistics")
		void IncrementHasTackled(EWtcTeam Team);

	UFUNCTION(BlueprintCallable, Category = "Statistics")
		void IncrementThrownEggs(EWtcTeam Team);

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Replicated, Category = "GameState|Counters")
		float CountdownTimer = 0.0f;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Replicated, Category = "GameState|Counters")
		float EndScreenTimer = 0.0f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Replicated, Category = "GameState|Counters")
		float CountdownDuration = 10.0f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Replicated, Category = "GameState|Counters")
		float EndScreenDuration = 30.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Replicated, Category = "End Game")
		bool bIsGameFinished = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Replicated, Category = "Countdown")
		bool bCountdownActive = false;
};
