// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Character.h"
#include "WtcTeam.h"
#include "WtcChicken.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FWtcChickenEvent);

UENUM(BlueprintType)
enum EWtcChickenState
{
	WTC_IS_IDLE UMETA(DisplayName = "Idle"),
	WTC_IS_SLEEPING UMETA(DisplayName = "Sleeping"),
	WTC_IS_RUNNING UMETA(DisplayName = "Running"),
	WTC_IS_FLYING	UMETA(DisplayName = "Flying"),
	WTC_IS_GOING_TO_PLAYER	UMETA(DisplayName = "Going to player"),
	WTC_IS_GOING_TO_BARNORFARM	UMETA(DisplayName = "Going to closest barn or farm"),
	WTC_IS_CARRIED UMETA(DisplayName = "Carried by Player")
};

UCLASS()
class WHATTHECLUCK_API AWtcChicken : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AWtcChicken();

	UFUNCTION(BlueprintImplementableEvent, Category = "General")
		void KillChicken();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	/*=============
	BASICS
	===============*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ChickenStats|Basics")
		float Weight = 3.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Meta = (ExposeOnSpawn = true), Replicated, Category = "ChickenStats|Basics")
		EWtcTeam Team = EWtcTeam::WTC_Neutral;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Replicated, Category = "ChickenStats|Basics")
		float SeduceValue = 0.1f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Replicated, Category = "ChickenStats|Basics")
		float OccupyValue = 0.1f;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "General")
		bool bIsSniped = false;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "General")
		bool bThrownOnBarnFarm = false;

	/*=============
	MOVEMENT
	===============*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ChickenStats|Movement")
		float WalkingSpeed = 50.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ChickenStats|Movement")
		float SprintSpeed = 700.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ChickenStats|Movement")
		float FlySpeed = 1200.0f;

	UPROPERTY(BlueprintAssignable, BlueprintCallable, BlueprintReadWrite, Category = "General")
		FWtcChickenEvent OnPickedUp;

	UPROPERTY(BlueprintAssignable, BlueprintCallable, BlueprintReadWrite, Category = "General")
		FWtcChickenEvent OnDropped;

	UPROPERTY(BlueprintAssignable, BlueprintCallable, BlueprintReadWrite, Category = "General")
		FWtcChickenEvent OnThrown;

private:
	/*=============
	STATE
	===============*/
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = "ChickenState")
		TEnumAsByte<EWtcChickenState> ChickenState = EWtcChickenState::WTC_IS_IDLE;

	//Do not change in blueprint (internal use only)
	float TimeInState = 0.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"), Category = "ChickenState")
		float TimeTillIdle = 8.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"), Category = "ChickenState")
		float TimeTillSleep = 30.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"), Category = "ChickenState")
		float TimeTillDespawn = 20.0f;

public:
	UFUNCTION(BlueprintCallable, Category = "ChickenState")
		void SetChickenState(EWtcChickenState NewChickenState);

	UFUNCTION(BlueprintCallable, Category = "ChickenState")
		EWtcChickenState GetChickenState();

	UFUNCTION(BlueprintNativeEvent, Category = "ChickenState")
		void OnChickenWantsToFlyAway();

	UFUNCTION(BlueprintNativeEvent, Category = "ChickenState")
		void OnChickenStateChanged();

	UFUNCTION(BlueprintNativeEvent, Category = "ChickenState")
		void OnChickenStateIsIdle();

	UFUNCTION(BlueprintNativeEvent, Category = "ChickenState")
		void OnChickenStateIsSleeping();

	UFUNCTION(BlueprintNativeEvent, Category = "ChickenState")
		void OnChickenStateIsRunning();

	UFUNCTION(BlueprintNativeEvent, Category = "ChickenState")
		void OnChickenStateIsFlying();

	UFUNCTION(BlueprintNativeEvent, Category = "ChickenState")
		void OnChickenStateIsCarried();

	UFUNCTION(BlueprintNativeEvent, Category = "ChickenState")
		void OnChickenStateIsGoingToPlayer();

	UFUNCTION(BlueprintNativeEvent, Category = "ChickenState")
		void OnChickenStateIsGoingToBarnOrFarm();
};
