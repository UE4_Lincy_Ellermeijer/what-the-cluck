// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Components/ActorComponent.h"
#include "WtcBaseAbilityComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FWtcAbilityEvent);

class AWtcCharacter;

UCLASS(Blueprintable, ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class WHATTHECLUCK_API UWtcBaseAbilityComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	UWtcBaseAbilityComponent();

protected:
	virtual void BeginPlay() override;

public:
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UFUNCTION(BlueprintCallable, Category = "General")
		AWtcCharacter* GetOwnerCharacter();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Replicated, Category = "General")
		bool IsEnabled = true;

	UPROPERTY(BlueprintAssignable, BlueprintCallable, BlueprintReadWrite, Replicated, Category = "General")
		FWtcAbilityEvent OnTryActivateAbility;

	UPROPERTY(BlueprintAssignable, BlueprintCallable, BlueprintReadWrite, Replicated, Category = "General")
		FWtcAbilityEvent OnActivateAbility;

	UPROPERTY(BlueprintAssignable, BlueprintCallable, BlueprintReadWrite, Replicated, Category = "General")
		FWtcAbilityEvent OnDeActivateAbility;

	UPROPERTY(BlueprintAssignable, BlueprintCallable, BlueprintReadWrite, Replicated, Category = "General")
		FWtcAbilityEvent OnSuccessfulActivation;

	UFUNCTION(BlueprintImplementableEvent, Category = "Ability")
		void ActivateAbility();

	UFUNCTION(BlueprintImplementableEvent, Category = "Ability")
		void DeActivateAbility();

	UFUNCTION(BlueprintImplementableEvent, Category = "Ability")
		bool CanUseAbility();

	UFUNCTION(BlueprintCallable, Category = "Ability")
		void AbilityButtonPressed();

	UFUNCTION(BlueprintCallable, Category = "Ability")
		void AbilityButtonReleased();
};
