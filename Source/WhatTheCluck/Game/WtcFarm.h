// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "WtcTeam.h"
#include "WtcChicken.h"
#include "WtcFarm.generated.h"

USTRUCT(BlueprintType)
struct FRoosterSniper
{
	GENERATED_BODY()
public:
	UPROPERTY()
		AWtcChicken* Rooster = nullptr;
	UPROPERTY()
		AActor* SniperReticleActor = nullptr;
	UPROPERTY()
		FTimerHandle SniperTimerHandle;
};

UCLASS()
class WHATTHECLUCK_API AWtcFarm : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AWtcFarm();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	/* The team that this spawn is for */
	UPROPERTY(EditAnywhere, Category = "Team", BlueprintReadWrite, Replicated)
		EWtcTeam Team;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Replicated, Category = "Egg")
		float EggRespawnTime = 2.0f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Replicated, Category = "Egg")
		int32 MaximumAmountOfEggs = 10;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Replicated, Category = "Egg")
		int32 CurrentAmountOfEggs = 0;

	FTimerHandle EggRespawnTimer;
	FTimerDelegate EggRespawnDelegate;

	UFUNCTION(BlueprintImplementableEvent, Category = "Egg")
		void SpawnEgg();

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Egg")
		TSubclassOf<AActor> EggActor;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Replicated, Category = "General")
		float MotherHenLife = 1.0f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "General")
		float TimeToSnipe = 3.0f;

	UPROPERTY()
		TArray<FRoosterSniper> PendingRoosterSnipers;

	UPROPERTY(EditDefaultsOnly, Category = "Sniper")
		TSubclassOf<AActor> SniperActorClass;

	UFUNCTION(BlueprintCallable, Category = "Sniper")
		void AddSniperToRooster(AWtcChicken* Rooster);

	UFUNCTION(BlueprintCallable, Category = "Sniper")
		void RemoveSniperFromRooster(AWtcChicken* Rooster);

	UFUNCTION(BlueprintCallable, Category = "Sniper")
		bool HasSniperAttached(AWtcChicken* Rooster);

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Rooster")
		TArray<AActor*> CurrentOverlappedActors;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Rooster")
		FVector RoosterTarget;

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = "General")
		void SignalGameModeEnd();

};
