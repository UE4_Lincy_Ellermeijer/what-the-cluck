// Fill out your copyright notice in the Description page of Project Settings.

#include "WhatTheCluck.h"
#include "WtcEgg.h"


// Sets default values
AWtcEgg::AWtcEgg()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AWtcEgg::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AWtcEgg::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

