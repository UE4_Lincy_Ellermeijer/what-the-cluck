// Fill out your copyright notice in the Description page of Project Settings.

#include "WhatTheCluck.h"
#include "WtcCharacter.h"
#include "UnrealNetwork.h"
#include "WtcMovementComponent.h"

UWtcMovementComponent::UWtcMovementComponent(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	bIgnoreBaseRotation = true;
	// regular constructor code
}

void UWtcMovementComponent::InitializeComponent()
{
	Super::InitializeComponent();
}

//Tick Comp
void UWtcMovementComponent::TickComponent(float DeltaTime,
										  enum ELevelTick TickType,
										  FActorComponentTickFunction *ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	AWtcCharacter* Character = Cast<AWtcCharacter>(GetOwner());
	ensure(Character);
	if (!Character->bCanUseStaminaAbilities)
	{
		MaxWalkSpeed = Character->CurrentMovementSpeed * SprintModifier * Character->ExhaustionModifier;
	}
	else
	{
		MaxWalkSpeed = Character->CurrentMovementSpeed * SprintModifier;
	}

	GravityScale = 1.0f * GravityModifier;
}

void UWtcMovementComponent::SetVelocity(FVector Direction) {
	Velocity = Direction;
}