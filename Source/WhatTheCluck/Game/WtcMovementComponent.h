// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/CharacterMovementComponent.h"
#include "WtcMovementComponent.generated.h"

/**
 * 
 */
UCLASS()
class WHATTHECLUCK_API UWtcMovementComponent : public UCharacterMovementComponent
{
	GENERATED_UCLASS_BODY()

protected:

	//Init
	virtual void InitializeComponent() override;

	//Tick
	virtual void TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction *ThisTickFunction) override;

public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Walking")
		float SprintModifier = 1.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Jumping")
		float GravityModifier = 1.0f;

	UFUNCTION(BlueprintCallable, Category = "Movement|Flying")
		void SetVelocity(FVector Direction);
};