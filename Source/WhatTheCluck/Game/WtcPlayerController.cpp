// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "WhatTheCluck.h"
#include "WtcPlayerController.h"
#include "WtcCharacter.h"
#include "UnrealNetwork.h"
#include "WtcGameState.h"
#include "WtcUtility.h"
#include "WtcGameInstance.h"

AWtcPlayerController::AWtcPlayerController()
{
	bShowMouseCursor = true;
	DefaultMouseCursor = EMouseCursor::Crosshairs;
}

AWtcCharacter* AWtcPlayerController::GetCharacterPawn()
{
	return Cast<AWtcCharacter>(GetPawn());
}

bool AWtcPlayerController::IsMovementAllowed() const
{
	AWtcGameState* const GameState = GetWorld()->GetGameState<AWtcGameState>();
	return GameState && GameState->bIsInPlayingState;
}

void AWtcPlayerController::SetPawn(APawn* InPawn)
{
	Super::SetPawn(InPawn);
	AWtcCharacter* CharacterPawn = Cast<AWtcCharacter>(InPawn);
	if (CharacterPawn)
	{
		CharacterPawn->Team = Team;
	}
	if (HUDWidgetClass)
	{
		HUDWidget = CreateWidget<UUserWidget>(this, HUDWidgetClass);
		if (HUDWidget)
		{
			HUDWidget->AddToPlayerScreen();
		}
	}
}

void AWtcPlayerController::PlayerTick(float DeltaTime)
{
	Super::PlayerTick(DeltaTime);

}

void AWtcPlayerController::SetupInputComponent()
{
	// set up gameplay key bindings
	Super::SetupInputComponent();
	// Action
	InputComponent->BindAction("AbilityOne", EInputEvent::IE_Pressed, this, &AWtcPlayerController::ActivateAbilityOne_Pressed);
	InputComponent->BindAction("AbilityOne", EInputEvent::IE_Released, this, &AWtcPlayerController::ActivateAbilityOne_Released);
	InputComponent->BindAction("AbilityTwo", EInputEvent::IE_Pressed, this, &AWtcPlayerController::ActivateAbilityTwo_Pressed);
	InputComponent->BindAction("AbilityTwo", EInputEvent::IE_Released, this, &AWtcPlayerController::ActivateAbilityTwo_Released);
	InputComponent->BindAction("AbilityThree", EInputEvent::IE_Pressed, this, &AWtcPlayerController::ActivateAbilityThree_Pressed);
	InputComponent->BindAction("AbilityThree", EInputEvent::IE_Released, this, &AWtcPlayerController::ActivateAbilityThree_Released);

	InputComponent->BindAction("Jump", EInputEvent::IE_Pressed, this, &AWtcPlayerController::JumpPressed);
	InputComponent->BindAction("Jump", EInputEvent::IE_Released, this, &AWtcPlayerController::JumpReleased);
	InputComponent->BindAction("Sprint", EInputEvent::IE_Pressed, this, &AWtcPlayerController::SprintPressed);
	InputComponent->BindAction("Sprint", EInputEvent::IE_Released, this, &AWtcPlayerController::SprintReleased);
	InputComponent->BindAction("Throw", EInputEvent::IE_Pressed, this, &AWtcPlayerController::ThrowPressed);
	InputComponent->BindAction("Throw", EInputEvent::IE_Released, this, &AWtcPlayerController::ThrowReleased);
	InputComponent->BindAction("Interact", EInputEvent::IE_Pressed, this, &AWtcPlayerController::InteractPressed);
	InputComponent->BindAction("Interact", EInputEvent::IE_Released, this, &AWtcPlayerController::InteractReleased);
	InputComponent->BindAction("Tackle", EInputEvent::IE_Pressed, this, &AWtcPlayerController::TacklePressed);
	InputComponent->BindAction("Tackle", EInputEvent::IE_Released, this, &AWtcPlayerController::TackleReleased);


	InputComponent->BindAxis("MoveForward", this, &AWtcPlayerController::MoveForward);
	InputComponent->BindAxis("MoveRight", this, &AWtcPlayerController::MoveRight);
	InputComponent->BindAxis("AimHorizontal", this, &AWtcPlayerController::AimHorizontal);
	InputComponent->BindAxis("AimVertical", this, &AWtcPlayerController::AimVertical);
}

void AWtcPlayerController::MoveForward(float Value)
{
	if (!IsMovementAllowed())
	{
		return;
	}
	// find out which way is forward
	const FRotator Rotation = GetControlRotation();
	const FRotator YawRotation(0, Rotation.Yaw, 0);

	// get forward vector
	const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
	AWtcCharacter* Character_Ptr = Cast<AWtcCharacter>(GetPawn());
	if (Character_Ptr)
	{
		Character_Ptr->AddMovementInput(Direction, Value);
	}
}

void AWtcPlayerController::MoveRight(float Value)
{
	if (!IsMovementAllowed())
	{
		return;
	}
	// find out which way is right
	const FRotator Rotation = GetControlRotation();
	const FRotator YawRotation(0, Rotation.Yaw, 0);

	// get right vector
	const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
	// add movement in that direction
	AWtcCharacter* Character_Ptr = Cast<AWtcCharacter>(GetPawn());
	if (Character_Ptr)
	{
		Character_Ptr->AddMovementInput(Direction, Value);
	}
}


void AWtcPlayerController::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	//Physics Gravity
	DOREPLIFETIME(AWtcPlayerController, Team);
}

void AWtcPlayerController::AimHorizontal(float Value)
{
	if (!IsMovementAllowed())
	{
		return;
	}
	float VerticalValue = GetInputAxisValue(TEXT("AimVertical"));
	FVector TempVector = FVector(Value, VerticalValue, 0.0f);
	APawn* ControlledPawn = GetPawn();
	if (!FVector::ZeroVector.Equals(TempVector) && ControlledPawn)
	{
		float Rad = FMath::Atan2(Value, VerticalValue);
		float Rot = FMath::RadiansToDegrees(Rad);
		ControlledPawn->SetActorRotation(FRotator(0.0f, Rot, 0.0f));
	}
}

void AWtcPlayerController::AimVertical(float Value)
{
	if (!IsMovementAllowed())
	{
		return;
	}
	float HorizontalValue = GetInputAxisValue(TEXT("AimHorizontal"));
	FVector TempVector = FVector(HorizontalValue, Value, 0.0f);
	APawn* ControlledPawn = GetPawn();
	if (!FVector::ZeroVector.Equals(TempVector) && ControlledPawn)
	{
		float Rad = FMath::Atan2(HorizontalValue, Value);
		float Rot = FMath::RadiansToDegrees(Rad);
		ControlledPawn->SetActorRotation(FRotator(0.0f, Rot, 0.0f));
	}
}

void AWtcPlayerController::ThrowPressed()
{
	if (!IsMovementAllowed())
	{
		return;
	}
	BP_ThrowPressed();
}

void AWtcPlayerController::ThrowReleased()
{
	if (!IsMovementAllowed())
	{
		return;
	}
	BP_ThrowReleased();
}

void AWtcPlayerController::InteractPressed()
{
	if (!IsMovementAllowed())
	{
		return;
	}
	BP_InteractPressed();
}

void AWtcPlayerController::InteractReleased()
{
	if (!IsMovementAllowed())
	{
		return;
	}
	BP_InteractReleased();
}

void AWtcPlayerController::JumpPressed()
{
	if (!IsMovementAllowed())
	{
		return;
	}
	OnJumpPressedDelegate.Broadcast();
	BP_JumpPressed();
}

void AWtcPlayerController::JumpReleased()
{
	if (!IsMovementAllowed())
	{
		return;
	}
	OnJumpReleasedDelegate.Broadcast();
	BP_JumpReleased();
}

void AWtcPlayerController::SprintPressed()
{
	if (!IsMovementAllowed())
	{
		return;
	}
	OnSprintPressedDelegate.Broadcast();
	BP_SprintPressed();
}

void AWtcPlayerController::SprintReleased()
{
	if (!IsMovementAllowed())
	{
		return;
	}
	OnSprintReleasedDelegate.Broadcast();
	BP_SprintReleased();
}

void AWtcPlayerController::TacklePressed()
{
	if (!IsMovementAllowed())
	{
		return;
	}
	BP_TacklePressed();
}

void AWtcPlayerController::TackleReleased()
{
	if (!IsMovementAllowed())
	{
		return;
	}
	BP_TackleReleased();
}

void AWtcPlayerController::ActivateAbilityOne_Pressed()
{
	if (!IsMovementAllowed())
	{
		return;
	}
	AWtcCharacter* Character_Ptr = Cast<AWtcCharacter>(GetPawn());
	if (Character_Ptr&& Character_Ptr->ActiveAbilityOne)
	{
		Character_Ptr->ActiveAbilityOne->ActivateAbility();
	}
}

void AWtcPlayerController::ActivateAbilityOne_Released()
{
	if (!IsMovementAllowed())
	{
		return;
	}
	AWtcCharacter* Character_Ptr = Cast<AWtcCharacter>(GetPawn());
	if (Character_Ptr&& Character_Ptr->ActiveAbilityOne)
	{
		Character_Ptr->ActiveAbilityOne->DeActivateAbility();
	}
}

void AWtcPlayerController::ActivateAbilityTwo_Pressed()
{
	if (!IsMovementAllowed())
	{
		return;
	}
	AWtcCharacter* Character_Ptr = Cast<AWtcCharacter>(GetPawn());
	if (Character_Ptr && Character_Ptr->ActiveAbilityTwo)
	{
		Character_Ptr->ActiveAbilityTwo->ActivateAbility();
	}
}

void AWtcPlayerController::ActivateAbilityTwo_Released()
{
	if (!IsMovementAllowed())
	{
		return;
	}
	AWtcCharacter* Character_Ptr = Cast<AWtcCharacter>(GetPawn());
	if (Character_Ptr && Character_Ptr->ActiveAbilityTwo)
	{
		Character_Ptr->ActiveAbilityTwo->DeActivateAbility();
	}
}

void AWtcPlayerController::ActivateAbilityThree_Pressed()
{
	if (!IsMovementAllowed())
	{
		return;
	}
	AWtcCharacter* Character_Ptr = Cast<AWtcCharacter>(GetPawn());
	if (Character_Ptr && Character_Ptr->ActiveAbilityThree)
	{
		Character_Ptr->ActiveAbilityThree->ActivateAbility();
	}
}

void AWtcPlayerController::ActivateAbilityThree_Released()
{
	if (!IsMovementAllowed())
	{
		return;
	}
	AWtcCharacter* Character_Ptr = Cast<AWtcCharacter>(GetPawn());
	if (Character_Ptr && Character_Ptr->ActiveAbilityThree)
	{
		Character_Ptr->ActiveAbilityThree->DeActivateAbility();
	}
}

void AWtcPlayerController::StartCountdown_Implementation()
{
	AWtcGameState* GameState = UWtcUtility::GetWtcGameState(GetWorld());
	bool bIsSolo = GameState && GameState->GameSize == EWtcGameSize::WTC_Solo;
	UWhatTheCluckGameInstance* GameInstance = Cast<UWhatTheCluckGameInstance>(GetGameInstance());
	if (CountdownWidgetClass && !GameInstance->CountdownWidget && !bIsSolo)
	{
		GameInstance->CountdownWidget = CreateWidget<UUserWidget>(GetWorld(), CountdownWidgetClass);
		GameInstance->CountdownWidget->AddToViewport();
	}
}

void AWtcPlayerController::ShowEndscreen_Implementation()
{
	UWhatTheCluckGameInstance* GameInstance = Cast<UWhatTheCluckGameInstance>(GetGameInstance());
	if (EndScreenWidgetClass && !GameInstance->EndScreenWidget)
	{
		GameInstance->EndScreenWidget = CreateWidget<UUserWidget>(GetWorld(), EndScreenWidgetClass);
		GameInstance->EndScreenWidget->AddToViewport();
	}
}

void AWtcPlayerController::StartPlaying_Implementation()
{
	UWhatTheCluckGameInstance* GameInstance = Cast<UWhatTheCluckGameInstance>(GetGameInstance());
	if (GameInstance->CountdownWidget)
	{
		GameInstance->CountdownWidget->RemoveFromParent();
	}
	if (FarmBarnHUDClass && !GameInstance->FarmBarnWidget)
	{
		GameInstance->FarmBarnWidget = CreateWidget<UUserWidget>(GetWorld(), FarmBarnHUDClass);
		GameInstance->FarmBarnWidget->AddToViewport();
		AWtcGameState* GameState = Cast<AWtcGameState>(GetWorld()->GetGameState());
		if (GameState && (GameState->GameSize == EWtcGameSize::WTC_Solo || GameState->GameSize == EWtcGameSize::WTC_OneVsOne))
		{
			GameInstance->FarmBarnWidget->SetRenderTranslation(FVector2D(0.0f, 400.0f));
		}
	}
}


