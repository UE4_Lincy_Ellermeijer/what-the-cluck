// Fill out your copyright notice in the Description page of Project Settings.

#include "WhatTheCluck.h"
#include "WtcChicken.h"
#include "UnrealNetwork.h"


// Sets default values
AWtcChicken::AWtcChicken()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AWtcChicken::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AWtcChicken::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	TimeInState += DeltaTime;
	//GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, FString::Printf(TEXT("%d"), (int32)GetChickenState()));

	if ((GetChickenState() == WTC_IS_SLEEPING) && (TimeInState >= TimeTillDespawn))
	{
		OnChickenWantsToFlyAway();
	}

	if ((GetChickenState() == WTC_IS_GOING_TO_BARNORFARM) && (TimeInState >= 5.0))
	{
		OnChickenWantsToFlyAway();
	}

	if ((GetChickenState() == WTC_IS_RUNNING) && (TimeInState >= TimeTillIdle))
	{
		SetChickenState(WTC_IS_IDLE);
	}

	if ((GetChickenState() == WTC_IS_IDLE) && (TimeInState >= TimeTillSleep))
	{
		SetChickenState(WTC_IS_SLEEPING);
	}
	
	if ((GetChickenState() == WTC_IS_GOING_TO_PLAYER) && (TimeInState >= TimeTillIdle))
	{
		SetChickenState(WTC_IS_IDLE);
	}
}

void AWtcChicken::SetChickenState(EWtcChickenState NewChickenState)
{
	ChickenState = NewChickenState;
	TimeInState = 0;
	OnChickenStateChanged();
}

EWtcChickenState AWtcChicken::GetChickenState()
{
	return AWtcChicken::ChickenState;
}

void AWtcChicken::OnChickenWantsToFlyAway_Implementation()
{
}

void AWtcChicken::OnChickenStateIsIdle_Implementation()
{
}

void AWtcChicken::OnChickenStateIsSleeping_Implementation()
{
}

void AWtcChicken::OnChickenStateIsRunning_Implementation()
{
}

void AWtcChicken::OnChickenStateIsFlying_Implementation()
{
}

void AWtcChicken::OnChickenStateIsCarried_Implementation()
{
}

void AWtcChicken::OnChickenStateIsGoingToPlayer_Implementation()
{
}

void AWtcChicken::OnChickenStateIsGoingToBarnOrFarm_Implementation()
{
}


void AWtcChicken::OnChickenStateChanged_Implementation()
{
	switch (ChickenState)
	{
	case WTC_IS_IDLE: OnChickenStateIsIdle(); break;
	case WTC_IS_SLEEPING: OnChickenStateIsSleeping(); break;
	case WTC_IS_RUNNING: OnChickenStateIsRunning(); break;
	case WTC_IS_FLYING: OnChickenStateIsFlying(); break;
	case WTC_IS_CARRIED: OnChickenStateIsCarried(); break;
	case WTC_IS_GOING_TO_PLAYER: OnChickenStateIsGoingToPlayer(); break;
	case WTC_IS_GOING_TO_BARNORFARM: OnChickenStateIsGoingToBarnOrFarm(); break;
	default: OnChickenStateIsIdle();
	}
}

void AWtcChicken::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AWtcChicken, Team); 
	DOREPLIFETIME(AWtcChicken, OccupyValue);
}