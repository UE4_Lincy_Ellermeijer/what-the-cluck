// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "WhatTheCluck.h"
#include "WtcCharacter.h"
#include "WtcMovementComponent.h"
#include "UnrealNetwork.h"

AWtcCharacter::AWtcCharacter(const FObjectInitializer& ObjectInitializer)
	:Super(ObjectInitializer.SetDefaultSubobjectClass<UWtcMovementComponent>(ACharacter::CharacterMovementComponentName))
{
	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Rotate character to moving direction
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;

	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->bAbsoluteRotation = true; // Don't want arm to rotate when character does
	CameraBoom->TargetArmLength = 800.f;
	CameraBoom->RelativeRotation = FRotator(-60.f, 0.f, 0.f);
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

										  // Create a camera...
	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	TopDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// Activate ticking in order to update the cursor every frame.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;
	bReplicates = true;

	StaminaCooldownDelegate.BindLambda([this]()
	{
		bCanRegenerateStamina = true;
	});
}

void AWtcCharacter::BeginPlay()
{
	Super::BeginPlay();
	if (ActiveAbilityOne_Class)
	{
		ActiveAbilityOne = NewObject<UWtcAbilityComponent>(this, ActiveAbilityOne_Class, TEXT("ActiveAbilityOneComponent"));
		ActiveAbilityOne->RegisterComponent();
	}
	if (ActiveAbilityTwo_Class)
	{
		ActiveAbilityTwo = NewObject<UWtcAbilityComponent>(this, ActiveAbilityTwo_Class, TEXT("ActiveAbilityTwoComponent"));
		ActiveAbilityTwo->RegisterComponent();
	}
	if (ActiveAbilityThree_Class)
	{
		ActiveAbilityThree = NewObject<UWtcAbilityComponent>(this, ActiveAbilityThree_Class, TEXT("ActiveAbilityThreeComponent"));
		ActiveAbilityThree->RegisterComponent();
	}
	if (PassiveAbilityOne_Class)
	{
		PassiveAbilityOne = NewObject<UWtcAbilityComponent>(this, PassiveAbilityOne_Class, TEXT("PassiveAbilityOneComponent"));
		PassiveAbilityOne->RegisterComponent();
	}
	if (PassiveAbilityTwo_Class)
	{
		PassiveAbilityTwo = NewObject<UWtcAbilityComponent>(this, PassiveAbilityTwo_Class, TEXT("PassiveAbilityTwoComponent"));
		PassiveAbilityTwo->RegisterComponent();
	}
	if (PickupAbility_Class)
	{
		PickupAbility = NewObject<UWtcBaseAbilityComponent>(this, PickupAbility_Class, TEXT("PickupAbility"));
		PickupAbility->RegisterComponent();
	}
	if (ThrowingAbility_Class)
	{
		ThrowingAbility = NewObject<UWtcBaseAbilityComponent>(this, ThrowingAbility_Class, TEXT("ThrowingAbility"));
		ThrowingAbility->RegisterComponent();
	}
	if (TackleAbility_Class)
	{
		TackleAbility = NewObject<UWtcBaseAbilityComponent>(this, TackleAbility_Class, TEXT("TackleAbility"));
		TackleAbility->RegisterComponent();
	}
	if (SprintAbility_Class)
	{
		SprintAbility = NewObject<UWtcBaseAbilityComponent>(this, SprintAbility_Class, TEXT("SprintAbility"));
		SprintAbility->RegisterComponent();
	}
	if (JumpAbility_Class)
	{
		JumpAbility = NewObject<UWtcBaseAbilityComponent>(this, JumpAbility_Class, TEXT("JumpAbility"));
		JumpAbility->RegisterComponent();
	}
}

void AWtcCharacter::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);
	RegenerateStamina(DeltaSeconds);
}

bool AWtcCharacter::RemoveStamina(float ValuePercent)
{
	if (Stamina <= 0.0f)
	{
		return false;
	}
	Stamina = FMath::Clamp(Stamina - ValuePercent, 0.0f, 1.0f);
	if (Stamina <= 0.0f)
	{
		bCanUseStaminaAbilities = false;
		ActivateLargeStaminaCooldown();
	}
	else
	{
		ActivateSmallStaminaCooldown();
	}
	OnStaminaChange.Broadcast();
	return true;
}

bool AWtcCharacter::AddStamina(float ValuePercent)
{
	if (Stamina >= 1.0f)
	{
		return false;
	}
	Stamina = FMath::Clamp(Stamina + ValuePercent, 0.0f, 1.0f);
	if (Stamina >= 1.0f)
	{
		bCanUseStaminaAbilities = true;
	}
	OnStaminaChange.Broadcast();
	return true;
}

void AWtcCharacter::RegenerateStamina(float DeltaSeconds)
{
	if (bCanRegenerateStamina)
	{
		AddStamina(StaminaRegen * DeltaSeconds);
	}
}

void AWtcCharacter::ActivateSmallStaminaCooldown()
{
	bCanRegenerateStamina = false;
	GetWorldTimerManager().SetTimer(StaminaCooldownTimer, StaminaCooldownDelegate, StaminaRegenSmallDelay, false);
}

void AWtcCharacter::ActivateLargeStaminaCooldown()
{
	bCanRegenerateStamina = false;
	GetWorldTimerManager().SetTimer(StaminaCooldownTimer, StaminaCooldownDelegate, StaminaRegenLargeDelay, false);
}

void AWtcCharacter::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	//Physics Gravity
	DOREPLIFETIME(AWtcCharacter, Team);
	DOREPLIFETIME(AWtcCharacter, Stamina);
	DOREPLIFETIME(AWtcCharacter, CurrentMovementSpeed);
	DOREPLIFETIME(AWtcCharacter, CanSprint);
	DOREPLIFETIME(AWtcCharacter, IsSprinting);
	DOREPLIFETIME(AWtcCharacter, bJumpPressed);
	DOREPLIFETIME(AWtcCharacter, ActiveAbilityOne);
	DOREPLIFETIME(AWtcCharacter, ActiveAbilityTwo);
	DOREPLIFETIME(AWtcCharacter, ActiveAbilityThree);
	DOREPLIFETIME(AWtcCharacter, PassiveAbilityOne);
	DOREPLIFETIME(AWtcCharacter, PassiveAbilityTwo);
	DOREPLIFETIME(AWtcCharacter, ThrowingAbility);
	DOREPLIFETIME(AWtcCharacter, PickupAbility);
	DOREPLIFETIME(AWtcCharacter, TackleAbility);
	DOREPLIFETIME(AWtcCharacter, SprintAbility);
	DOREPLIFETIME(AWtcCharacter, JumpAbility);

}
