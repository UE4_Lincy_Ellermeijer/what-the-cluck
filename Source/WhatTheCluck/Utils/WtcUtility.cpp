#include "WhatTheCluck.h"
#include "Networking.h"
#include "Sockets.h"
#include "SocketSubsystem.h"
#include "WtcViewportClient.h"
#include "WtcGameInstance.h"
#include "WtcUtility.h"
#include "WtcGameState.h"
#include "WtcGameInstance.h"
#include "WtcGameMode.h"

FString UWtcUtility::GetLocalIPAddress()
{
	ISocketSubsystem* SocketSubsystem = ISocketSubsystem::Get(PLATFORM_SOCKETSUBSYSTEM);
	if (SocketSubsystem)
	{
		bool BindAll = false;
		TSharedRef<FInternetAddr> LocalAddress = SocketSubsystem->GetLocalHostAddr(*GLog, BindAll);
		if (LocalAddress->IsValid())
		{
			return LocalAddress->ToString(false);
		}
	}

	return TEXT("127.0.0.1");
}

float UWtcUtility::CalculateJumpVelocityToHeight(float DesiredHeight, float Gravity)
{
	return -FMath::Sqrt(2 * Gravity * DesiredHeight);
}

bool UWtcUtility::IsPlayerControllerExistingForId(UObject* WorldContextObject, int ControllerId)
{
	UWorld* World = GEngine->GetWorldFromContextObject(WorldContextObject);
	ULocalPlayer* LocalPlayer = GEngine->GetLocalPlayerFromControllerId(World, ControllerId);
	if (LocalPlayer)
	{
		return true;
	}
	return false;
}

APlayerController* UWtcUtility::GetPlayerControllerForID(UObject* WorldContextObject, int ControllerId)
{
	UWorld* World = GEngine->GetWorldFromContextObject(WorldContextObject);
	for (FConstPlayerControllerIterator Iterator = World->GetPlayerControllerIterator(); Iterator; ++Iterator)
	{
		APlayerController* PlayerController = Iterator->Get();
		if (PlayerController && Iterator.GetIndex() == ControllerId)
		{
			return PlayerController;
		}
	}
	return nullptr;
}

void UWtcUtility::SetSplitscreenDisabled(UObject * WorldContextObject, bool IsDisabled)
{
	UWorld* World = GEngine->GetWorldFromContextObject(WorldContextObject);
	if (World)
	{
		UWtcViewportClient* Viewport = Cast<UWtcViewportClient>(World->GetGameViewport());
		Viewport->SetDisableSplitscreen(IsDisabled);
	}
}

void UWtcUtility::SetGameSize(UObject* WorldContextObject, EWtcGameSize GameSize)
{
	UWorld* World = GEngine->GetWorldFromContextObject(WorldContextObject);
	UWhatTheCluckGameInstance* GameInstance = Cast<UWhatTheCluckGameInstance>(World->GetGameInstance());
	if (GameInstance)
	{
		GameInstance->GameSize = GameSize;
	}
}

AWtcGameState* UWtcUtility::GetWtcGameState(UObject* WorldContextObject)
{
	UWorld* World = GEngine->GetWorldFromContextObject(WorldContextObject);
	if (World)
	{
		return World->GetGameState<AWtcGameState>();
	}
	return nullptr;
}

AWtcGameMode* UWtcUtility::GetWtcGameMode(UObject* WorldContextObject)
{
	UWorld* World = GEngine->GetWorldFromContextObject(WorldContextObject);
	if (World)
	{
		return World->GetAuthGameMode<AWtcGameMode>();
	}
	return nullptr;
}

UWhatTheCluckGameInstance* UWtcUtility::GetWtcGameInstance(UObject* WorldContextObject)
{
	UWorld* World = GEngine->GetWorldFromContextObject(WorldContextObject);
	if (World)
	{
		return Cast<UWhatTheCluckGameInstance>(World->GetGameInstance());
	}
	return nullptr;
}
