// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "WtcGameSize.generated.h"

UENUM(BlueprintType)
enum class EWtcGameSize : uint8
{
	WTC_Solo UMETA(DisplayName = "Solo"),
	WTC_OneVsOne UMETA(DisplayName = "1vs1"),
	WTC_TwoVsTwo UMETA(DisplayName = "2vs2"),
	WTC_ThreeVsThree UMETA(DisplayName = "3vs3"),
	WTC_FourVsFour UMETA(DisplayName = "4vs4"),
	WTC_NONE UMETA(DisplayName = "None")
};
