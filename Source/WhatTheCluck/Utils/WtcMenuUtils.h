// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Kismet/BlueprintFunctionLibrary.h"
#include "WtcMenuUtils.generated.h"

UENUM(BlueprintType)
enum class EWtcMenuNavigation : uint8
{
	WTC_TOP UMETA(DisplayName = "Top"),
	WTC_BOTTOM UMETA(DisplayName = "Bottom"),
	WTC_RIGHT UMETA(DisplayName = "Right"),
	WTC_Left UMETA(DisplayName = "Left"),
	WTC_NONE UMETA(DisplayName = "None")
};


UCLASS()
class WHATTHECLUCK_API UWtcMenuUtils : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
public:
	UFUNCTION(BlueprintCallable, Category = "Players", meta = (WorldContext = "WorldContextObject"))
		static void RemoveInactivePlayers(UObject* WorldContextObject);

};
