// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "WtcTeam.generated.h"

UENUM(BlueprintType)
enum class EWtcTeam : uint8
{
	WTC_Neutral	UMETA(DisplayName = "Neutral"),
	WTC_Blue	UMETA(DisplayName = "Blue"),
	WTC_Red		UMETA(DisplayName = "Red")
};
