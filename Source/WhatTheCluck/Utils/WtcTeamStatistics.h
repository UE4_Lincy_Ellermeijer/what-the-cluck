// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "WtcTeamStatistics.generated.h"

/**
 * Contains all statistics for a team 
 */
USTRUCT(BlueprintType)
struct FWtcTeamStatistics
{
	GENERATED_BODY()
public:

	UPROPERTY(EditAnywhere, Category = "Statistics", BlueprintReadWrite)
		int32 AbilitiesUsed = 0;

	UPROPERTY(EditAnywhere, Category = "Statistics", BlueprintReadWrite)
		int32 PickedUpEggs = 0;

	UPROPERTY(EditAnywhere, Category = "Statistics", BlueprintReadWrite)
		int32 ThrownEggs = 0;

	UPROPERTY(EditAnywhere, Category = "Statistics", BlueprintReadWrite)
		int32 GotTackled = 0;

	UPROPERTY(EditAnywhere, Category = "Statistics", BlueprintReadWrite)
		int32 HasTackled = 0;
	
};
