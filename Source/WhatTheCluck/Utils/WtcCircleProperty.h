// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "WtcCircleProperty.generated.h"

USTRUCT(BlueprintType)
struct WHATTHECLUCK_API FWtcCircleProperty
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "General")
		FLinearColor Color = FColor::Red;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "General")
		float Alpha = 1.0f;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "General")
		FVector2D Scale = FVector2D(1.0f, 1.0f);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "General")
		float Thickness = 1.0f;

	
		
};
