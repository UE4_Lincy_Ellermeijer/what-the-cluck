#pragma once

DECLARE_LOG_CATEGORY_EXTERN(LogStateMachine, Log, All);

template <typename TStateType, typename TEventType>
class TWtcStateMachine
{
public:

	TStateType CurrentState;
	TEventType InitialEvent;

	TStateType PreviousState;

	/***********************************************************************
	 Function Delegates                                                   
	***********************************************************************/
	typedef TBaseDelegate<void, TStateType, TEventType> OnEnterStateDelegate;
	typedef TBaseDelegate<void, TStateType, TEventType> OnLeaveStateDelegate;
	typedef TBaseDelegate<void, TEventType> OnHandleEventDelegate;
	typedef TBaseDelegate<void, float> OnUpdateStateDelegate;
	typedef TBaseDelegate<bool> Conditional;

	/***********************************************************************
	 Transition Class                                                     
	***********************************************************************/
	class WtcStateMachineTransition
	{
	public:
		WtcStateMachineTransition(TStateType TargetState, TEventType TriggerEvent, Conditional Condition = Conditional()) :
			TriggeringEvent(TriggerEvent), DestinationState(TargetState), CanEnterConditional(Condition)
		{
		}

		TEventType TriggeringEvent;
		TStateType DestinationState;
		Conditional CanEnterConditional;

		/***********************************************************************
		 Return weather this Transition can be entered
		 Checks the Event and Conditional(if set)
		***********************************************************************/
		bool ProcessEvent(TEventType IncomingEvent)
		{
			return TriggeringEvent == IncomingEvent && (!CanEnterConditional.IsBound() || CanEnterConditional.Execute());
		}

	};

	/***********************************************************************
	 State Class                                                          
	***********************************************************************/
	class WtcStateMachineState
	{
		OnEnterStateDelegate OnEnterDelegate;
		OnLeaveStateDelegate OnLeaveDelegate;
		OnUpdateStateDelegate OnUpdateDelegate;

		// Transitions for this state
		TArray<WtcStateMachineTransition> Transitions;
		// Event Handler Map for in State Event handling
		TMap<TEventType, OnHandleEventDelegate> EventHandlers;
	public:
		TStateType StateType;
		WtcStateMachineState() = default;

		WtcStateMachineState(TStateType InStateType) :
			StateType(InStateType)
		{
		}

		/***********************************************************************
		 Create a Transition for this State                                   
		***********************************************************************/
		WtcStateMachineState& Transition(TStateType TargetState, TEventType TriggerEvent, Conditional Condition = Conditional())
		{
			Transitions.Add(WtcStateMachineTransition(TargetState, TriggerEvent, Condition));
			return *this;
		}

		/***********************************************************************
		 Create a Transition to Re-Enter this State                           
		***********************************************************************/
		WtcStateMachineState& ReEnter(TEventType TriggerEvent, Conditional CanEnter = Conditional())
		{
			Transitions.Add(WtcStateMachineTransition(StateType, TriggerEvent, CanEnter));
			return *this;
		}

		/***********************************************************************
		 Set the On-Enter State Delegate                                      
		***********************************************************************/
		WtcStateMachineState& OnEnter(OnEnterStateDelegate EnterDelegate)
		{
			OnEnterDelegate = EnterDelegate;
			return *this;
		}

		/***********************************************************************
		 Set the On-Leave State Delegate                                      
		***********************************************************************/
		WtcStateMachineState& OnLeave(OnLeaveStateDelegate LeaveDelegate)
		{
			OnLeaveDelegate = LeaveDelegate;
			return *this;
		}
		/***********************************************************************
		 Set the On-Update State Delegate                                     
		***********************************************************************/
		WtcStateMachineState& OnUpdate(OnUpdateStateDelegate UpdateDelegate)
		{
			OnUpdateDelegate = UpdateDelegate;
			return *this;
		}
		/***********************************************************************
		 Add an custom EventHandler Delegate
		 This will call the Delegate on the Event and no Transition is found
		***********************************************************************/
		WtcStateMachineState& HandleEvent(TEventType Event, OnHandleEventDelegate EventDelegate)
		{
			EventHandlers.Add(Event, EventDelegate);
			return *this;
		}

		/***********************************************************************
		 This will process Events
		 If a Transition is found for this Event and this Transition can be entered, return the Transition
		 Else look for fitting EventHandlers and execute any available and return no Transition(nullptr)
		***********************************************************************/
		WtcStateMachineTransition* ProcessEvent(TEventType IncomingEvent)
		{
			for (int32 Index = 0; Index < Transitions.Num(); Index++)
			{
				WtcStateMachineTransition* CurrentTransition = &Transitions[Index];
				if (CurrentTransition->TriggeringEvent == IncomingEvent && CurrentTransition->ProcessEvent(IncomingEvent))
				{
					return CurrentTransition;
				}
			}
			OnHandleEventDelegate* Delegate = EventHandlers.Find(IncomingEvent);
			if (Delegate)
			{
				Delegate->ExecuteIfBound(IncomingEvent);
			}
			return nullptr;
		}

		// Execute OnEnter Delegate
		void Enter(TStateType InStateType, TEventType EventType)
		{
			OnEnterDelegate.ExecuteIfBound(InStateType, EventType);
		}

		// Execute OnUpdate Delegate
		void Update(float DeltaSeconds)
		{
			OnUpdateDelegate.ExecuteIfBound(DeltaSeconds);
		}

		// Execute OnLeave Delegate
		void Leave(TStateType InStateType, TEventType EventType)
		{
			OnLeaveDelegate.ExecuteIfBound(InStateType, EventType);
		}

	};

	/***********************************************************************
	 Create a new State with the given StateType and return it
	 States will only be added if not existing before
	***********************************************************************/
	WtcStateMachineState& State(TStateType InStateType)
	{
		WtcStateMachineState NewState(InStateType);
		WtcStateMachineState* ExistingState = States.Find(NewState);
		if (!ExistingState)
		{
			bool AlreadyAdded = false;
			auto ElementID = States.Add(NewState, &AlreadyAdded);
			ensureMsgf(!AlreadyAdded, TEXT("State was already added but we could not find it previously"));
			ExistingState = &States[ElementID];
		}
		return *ExistingState;
	}


	TWtcStateMachine() = default;

	TWtcStateMachine(TStateType InInitialState, TEventType InInitialEvent) :
		CurrentState(InInitialState),
		InitialEvent(InInitialEvent)
	{
	}

	/***********************************************************************
	 StateMachine General Update Function
	 Will Enter Initial State with Initial Event for the first call of this function
	 After that each Tick, Latent Events will be processed and after that
	 the current States Update Delegate called
	***********************************************************************/
	void Update(float DeltaSeconds)
	{
		if (!bHasEnteredInitialState)
		{
			State(CurrentState).Enter(CurrentState, InitialEvent);
			bHasEnteredInitialState = true;
		}

		TEventType PendingEvent;
		while (PendingEvents.Dequeue(PendingEvent))
		{
			ProcessEvent(PendingEvent);
		}

		State(CurrentState).Update(DeltaSeconds);
	}

	// Queue a Latent Event, will be processed in StateMachine Update Function in the next Call
	void NotifyLatent(TEventType Event)
	{
		PendingEvents.Enqueue(Event);
	}

	// Immediately process Event
	void NotifyImmediate(TEventType Event)
	{
		ProcessEvent(Event);
	}

	/***********************************************************************
	 Processes Event sent to the StateMachine
	 Checks for possible Transitions and calls On-Leave and On-Enter Delegates
	 and sets current and previous States
	***********************************************************************/
	void ProcessEvent(TEventType IncomingEvent)
	{
		WtcStateMachineTransition* TransitionToPerform = nullptr;
		TransitionToPerform = State(CurrentState).ProcessEvent(IncomingEvent);
		if (TransitionToPerform != nullptr)
		{
			State(CurrentState).Leave(TransitionToPerform->DestinationState, IncomingEvent);
			PreviousState = CurrentState;
			CurrentState = TransitionToPerform->DestinationState;
			State(CurrentState).Enter(PreviousState, IncomingEvent);
		}
	}

	~TWtcStateMachine() = default;

	// Necessary template to provide Key-Value Functionality to TSets
	// Used for the State Set, so there are no duplicate States
	template<typename ElementType, bool bInAllowDuplicateKeys = false>
	struct StateKeyFuncs : BaseKeyFuncs<ElementType, ElementType, bInAllowDuplicateKeys>
	{
		typedef typename TCallTraits<ElementType>::ParamType KeyInitType;
		typedef typename TCallTraits<ElementType>::ParamType ElementInitType;

		/**
		* @return The key used to index the given element.
		*/
		static FORCEINLINE KeyInitType GetSetKey(ElementInitType Element)
		{
			return Element;
		}

		/**
		* @return True if the states match.
		*/
		static FORCEINLINE bool Matches(KeyInitType A, KeyInitType B)
		{
			return A.StateType == B.StateType;
		}

		/** Return the state as hash */
		static FORCEINLINE uint32 GetKeyHash(KeyInitType Key)
		{
			return static_cast<uint32>(Key.StateType);
		}
	};

private:
	bool bHasEnteredInitialState = false;
	TQueue<TEventType> PendingEvents;
	TSet<WtcStateMachineState, StateKeyFuncs<WtcStateMachineState>> States;
};
