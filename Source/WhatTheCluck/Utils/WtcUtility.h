#include "WhatTheCluck.h"
#include "WtcGameSize.h"
#include "WtcUtility.generated.h"

class AWtcGameState;

UCLASS()
class WHATTHECLUCK_API UWtcUtility : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
public:

	UFUNCTION(BlueprintCallable, Category = "Network")
		static FString GetLocalIPAddress();

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Movement")
		static float CalculateJumpVelocityToHeight(float DesiredHeight, float Gravity);

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Menu", meta = (WorldContext = "WorldContextObject"))
		static bool IsPlayerControllerExistingForId(UObject* WorldContextObject, int ControllerId);

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Menu", meta = (WorldContext = "WorldContextObject"))
		static APlayerController* GetPlayerControllerForID(UObject* WorldContextObject, int ControllerId);

	UFUNCTION(BlueprintCallable, Category = "Menu", meta = (WorldContext = "WorldContextObject"))
		static void SetSplitscreenDisabled(UObject* WorldContextObject, bool IsEnabled);

	UFUNCTION(BlueprintCallable, Category = "GameSize", meta = (WorldContext = "WorldContextObject"))
		static void SetGameSize(UObject* WorldContextObject, EWtcGameSize GameSize);

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "General", meta = (WorldContext = "WorldContextObject"))
		static AWtcGameState* GetWtcGameState(UObject* WorldContextObject);

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "General", meta = (WorldContext = "WorldContextObject"))
		static UWhatTheCluckGameInstance* GetWtcGameInstance(UObject* WorldContextObject);

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "General", meta = (WorldContext = "WorldContextObject"))
		static AWtcGameMode* GetWtcGameMode(UObject* WorldContextObject);

};
