// Fill out your copyright notice in the Description page of Project Settings.

#include "WhatTheCluck.h"
#include "WtcMenuPlayerController.h"
#include "WtcMenuUtils.h"

void UWtcMenuUtils::RemoveInactivePlayers(UObject* WorldContextObject)
{
	UWorld* World = GEngine->GetWorldFromContextObject(WorldContextObject);
	TArray<AWtcMenuPlayerController*> ControllersToRemove;
	for (FConstPlayerControllerIterator Iterator = World->GetPlayerControllerIterator(); Iterator; ++Iterator)
	{
		AWtcMenuPlayerController* PlayerController = Cast<AWtcMenuPlayerController>(Iterator->Get());
		if (PlayerController && !PlayerController->IsActive)
		{
			ControllersToRemove.Add(PlayerController);
		}
	}
	for (int Index = 0; Index < ControllersToRemove.Num(); ++Index)
	{
		UGameplayStatics::RemovePlayer(ControllersToRemove[Index], true);
		UE_LOG(LogTemp, Warning, TEXT("Removed Player"));
	}
}
