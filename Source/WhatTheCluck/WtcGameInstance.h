// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "Engine/GameInstance.h"
#include "WtcGameSize.h"
#include "UMG.h"
#include "WtcGameInstance.generated.h"

/*
GameInstance exists on the server and on each client. The instances do not communicate with each other.
This is a good place to store certain types of persistent data such as lifetime player statistics (e.g. total number of games won).
*/
UCLASS()
class WHATTHECLUCK_API UWhatTheCluckGameInstance : public UGameInstance
{
	GENERATED_BODY()


public:

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Player")
		TArray<APlayerController*> LocalPlayerControllers;

	UPROPERTY(BlueprintReadWrite, Category = "General")
		EWtcGameSize GameSize = EWtcGameSize::WTC_Solo;

	UPROPERTY()
		UUserWidget* CountdownWidget;
	UPROPERTY()
		UUserWidget* FarmBarnWidget;
	UPROPERTY()
		UUserWidget* EndScreenWidget;

};
