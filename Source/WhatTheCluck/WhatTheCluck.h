// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#ifndef __WHATTHECLUCK_H__
#define __WHATTHECLUCK_H__

#include "Engine.h"

DECLARE_LOG_CATEGORY_EXTERN(LogWhatTheCluck, Log, All);

#define TO_STRING2(Value) #Value
#define TO_STRING(Value) TO_STRING2(Value)

#define BIND_LAMBDA(Type, Lambda) \
  Type::CreateLambda( Lambda )

#define BIND_UOBJECT_DELEGATE(Type, Function) \
	Type::CreateUObject( this, &ThisClass::Function )

#endif
