// Fill out your copyright notice in the Description page of Project Settings.

#include "WhatTheCluck.h"
#include "WtcMenuPlayerController.h"
#include "WtcViewportClient.h"

bool UWtcViewportClient::InputKey(FViewport* InViewport, int32 ControllerId, FKey Key, EInputEvent EventType, float AmountDepressed, bool bGamepad)
{
	if (bGamepad)
	{
		if (EventType == EInputEvent::IE_Pressed && Key == EKeys::Gamepad_Special_Right)
		{
			ULocalPlayer* LocalPlayer = GEngine->GetLocalPlayerFromControllerId(GetWorld(), ControllerId);
			AWtcMenuPlayerController* PlayerController = Cast<AWtcMenuPlayerController>(LocalPlayer->GetPlayerController(GetWorld()));
			if (PlayerController && !PlayerController->IsActive)
			{
				PlayerController->IsActive = true;
			}
		}
		return Super::InputKey(InViewport, ControllerId, Key, EventType, AmountDepressed, bGamepad);
	}
	else
	{
		return Super::InputKey(InViewport, ControllerId, Key, EventType, AmountDepressed, bGamepad);
	}
}

bool UWtcViewportClient::InputAxis(FViewport* InViewport, int32 ControllerId, FKey Key, float Delta, float DeltaTime, int32 NumSamples /*= 1*/, bool bGamepad /*= false*/)
{
	if (bGamepad)
	{

		return Super::InputAxis(InViewport, ControllerId, Key, Delta, DeltaTime, NumSamples, bGamepad);
	}
	else
	{
		return Super::InputAxis(InViewport, ControllerId, Key, Delta, DeltaTime, NumSamples, bGamepad);
	}
}

void UWtcViewportClient::SetDisableSplitscreen(bool IsEnabled)
{
	SetDisableSplitscreenOverride(IsEnabled);
}
