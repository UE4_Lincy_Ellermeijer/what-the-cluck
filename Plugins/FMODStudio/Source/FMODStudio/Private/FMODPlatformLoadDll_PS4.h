// Copyright (c), Firelight Technologies Pty, Ltd. 2012-2017.
#pragma once

#include "fmodorbis.h"

void* FMODPlatformLoadDll(const TCHAR* LibToLoad)
{
	return FPlatformProcess::GetDllHandle(LibToLoad);
}

FMOD_RESULT FMODPlatformSystemSetup()
{
	FMOD_ORBIS_THREADAFFINITY Affinity = { 0 };
	Affinity.mixer = FMOD_THREAD_CORE4;
	Affinity.studioUpdate = FMOD_THREAD_CORE5;
#if FMOD_VERSION >= 0x00010800
	Affinity.studioLoadBank = FMOD_THREAD_CORE5;
	Affinity.studioLoadSample = FMOD_THREAD_CORE5;
#else
	Affinity.studioLoad = FMOD_THREAD_CORE5;
#endif
	return FMOD_Orbis_SetThreadAffinity(&Affinity);
}
